'use strict';

angular.module('slaGenerator').factory('ThreatAgentService',
		['$sessionStorage','$localStorage', '$http', '$q', 'urls',
			function ($sessionStorage, $localStorage, $http, $q, urls) {

			var factory = {
					getQuestions : getQuestions,
					getReplies: getReplies,
					submitQuestions : submitQuestions,
				getAttributes:getAttributes,
				submitEvaluation:submitEvaluation
			};

			return factory;

			
			function getQuestions(){
				var deferred = $q.defer();
				$http(
						{
							method : 'GET',
							url : urls.THREATAGENT_API+"/questions"
						})
						.then(
								function successCallback(
										response) {
									deferred.resolve(response.data);
								},
								function errorCallback(
										errResponse) {
									deferred.reject(errResponse);
								});

				return deferred.promise;
			}

				function getReplies()
				{
					var deferred = $q.defer();
					$http(
						{
							method : 'GET',
							url : urls.THREATAGENT_API+"/replies"
						})
						.then(
							function successCallback(
								response) {
								deferred.resolve(response.data);
							},
							function errorCallback(
								errResponse) {
								deferred.reject(errResponse);
							});

					return deferred.promise;
				}

				function getAttributes()
				{
					var deferred = $q.defer();
					$http(
						{
							method : 'GET',
							url : urls.THREATAGENT_API+"/attributes"
						})
						.then(
							function successCallback(
								response) {
								deferred.resolve(response.data);
							},
							function errorCallback(
								errResponse) {
								deferred.reject(errResponse);
							});

					return deferred.promise;
				}

				function submitQuestions(appId, threatagentReply){
					var deferred = $q.defer();

					$http(
						{
							method : 'POST',
							url : urls.THREATAGENT_API+"/submitQuestions",
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							transformRequest : function(
								obj) {
								var str = [];
								for ( var p in obj)
									str
										.push(encodeURIComponent(p)
											+ "="
											+ encodeURIComponent(obj[p]));
								return str.join("&");
							},
							data : {
								'appId' : appId,
								'threatagentReply' : JSON.stringify(threatagentReply),

							}

						})
						.then(
							function successCallback(
								response) {
								deferred.resolve(response.data);
							},
							function errorCallback(
								errResponse) {
								deferred.reject(errResponse);
							});

					return deferred.promise;
					
				}


				function submitEvaluation(appId, CategoryScoring){
					var deferred = $q.defer();

					$http(
						{
							method : 'POST',
							url : urls.THREATAGENT_API+"/submitScoring",
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							transformRequest : function(
								obj) {
								var str = [];
								for ( var p in obj)
									str
										.push(encodeURIComponent(p)
											+ "="
											+ encodeURIComponent(obj[p]));
								return str.join("&");
							},
							data : {
								'appId' : appId,
								'CategoryScoring' : JSON.stringify(CategoryScoring),

							}

						})
						.then(
							function successCallback(
								response) {
								deferred.resolve(response.data);
							},
							function errorCallback(
								errResponse) {
								deferred.reject(errResponse);
							});

					return deferred.promise;

				}



		}
		]);