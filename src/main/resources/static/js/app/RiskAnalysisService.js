'use strict';

angular.module('slaGenerator').factory('RiskAnalysisService',
		['$sessionStorage','$localStorage', '$http', '$q', 'urls',
			function ($sessionStorage, $localStorage, $http, $q, urls) {

			var factory = {
					getComponentQuestions : getComponentQuestions,
					submitQuestions : submitQuestions,
					saveEvaluation : saveEvaluation,
					submitControls : submitControls,
				    calculateScores:calculateScores,
				    saveImpactEvaluation:saveImpactEvaluation
			};

			return factory;

			
			function getComponentQuestions(componentId){
				var deferred = $q.defer();

				$http(
						{
							method : 'GET',
							url : urls.RISKANALYSIS+"/componentQuestions?componentId="+componentId
						})
						.then(
								function successCallback(
										response) {
									deferred.resolve(response.data);
								},
								function errorCallback(
										errResponse) {
									deferred.reject(errResponse);
								});

				return deferred.promise;
			}
			
			function submitQuestions(appId,compId, questions){
				var deferred = $q.defer();

				$http(
						{
							method : 'POST',
							url : urls.RISKANALYSIS+"/submitQuestions",
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							transformRequest : function(
									obj) {
								var str = [];
								for ( var p in obj)
									str
									.push(encodeURIComponent(p)
											+ "="
											+ encodeURIComponent(obj[p]));
								return str.join("&");
							},
							data : {
								'appId':appId,
								'compId' : compId,
								'threatQuestions' : JSON.stringify(questions),
							}

						})
						.then(
								function successCallback(
										response) {
									deferred.resolve(response.data);
								},
								function errorCallback(
										errResponse) {
									deferred.reject(errResponse);
								});

				return deferred.promise;
			}






				function saveImpactEvaluation(appId,compId,impactScores){
					var deferred = $q.defer();

					$http(
						{
							method : 'POST',
							url : urls.RISKANALYSIS+"/saveImpactEvaluation",
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							transformRequest : function(
								obj) {
								var str = [];
								for ( var p in obj)
									str
										.push(encodeURIComponent(p)
											+ "="
											+ encodeURIComponent(obj[p]));
								return str.join("&");
							},
							data : {
								'appId' : appId,
								'compId' :compId,
								'impactScores':JSON.stringify(impactScores)

							}

						})
						.then(
							function successCallback(
								response) {
								deferred.resolve(response.data);
							},
							function errorCallback(
								errResponse) {
								deferred.reject(errResponse);
							});

					return deferred.promise;
				}


				function calculateScores(appId){
					var deferred = $q.defer();

					$http(
						{
							method : 'POST',
							url : urls.RISKANALYSIS+"/calculateScores",
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							transformRequest : function(
								obj) {
								var str = [];
								for ( var p in obj)
									str
										.push(encodeURIComponent(p)
											+ "="
											+ encodeURIComponent(obj[p]));
								return str.join("&");
							},
							data : {
								'appId' : appId,

							}

						})
						.then(
							function successCallback(
								response) {
								deferred.resolve(response.data);
							},
							function errorCallback(
								errResponse) {
								deferred.reject(errResponse);
							});

					return deferred.promise;
				}
			
			function saveEvaluation(compId, componentThreats, spoofingRisk, tamperingRisk, repudiationRisk, informationDisclosureRisk, denialOfServiceRisk, elevationOfPrivilegesRisk){
				var deferred = $q.defer();

				$http(
						{
							method : 'POST',
							url : urls.RISKANALYSIS+"/saveEvaluation",
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							transformRequest : function(
									obj) {
								var str = [];
								for ( var p in obj)
									str
									.push(encodeURIComponent(p)
											+ "="
											+ encodeURIComponent(obj[p]));
								return str.join("&");
							},
							data : {
								'compId' : compId,
								'componentThreats' : JSON.stringify(componentThreats),
								'spoofingRisk' : spoofingRisk,
								'tamperingRisk' : tamperingRisk,
								'repudiationRisk' : repudiationRisk,
								'informationDisclosureRisk' : informationDisclosureRisk,
								'denialOfServiceRisk' : denialOfServiceRisk,
								'elevationOfPrivilegesRisk' : elevationOfPrivilegesRisk
							}

						})
						.then(
								function successCallback(
										response) {
									deferred.resolve(response.data);
								},
								function errorCallback(
										errResponse) {
									deferred.reject(errResponse);
								});

				return deferred.promise;
			}
			
			function submitControls(compId, controls, musaJwtToken, musaUserId){
				var deferred = $q.defer();

				$http(
						{
							method : 'POST',
							url : urls.RISKANALYSIS+"/submitControls",
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							transformRequest : function(
									obj) {
								var str = [];
								for ( var p in obj)
									str
									.push(encodeURIComponent(p)
											+ "="
											+ encodeURIComponent(obj[p]));
								return str.join("&");
							},
							data : {
								'compId' : compId,
								'controls' : JSON.stringify(controls),
								'jwtToken' : musaJwtToken,
								'userId' : musaUserId,
							}

						})
						.then(
								function successCallback(
										response) {
									deferred.resolve(response.data);
								},
								function errorCallback(
										errResponse) {
									deferred.reject(errResponse);
								});

				return deferred.promise;
			}

		}
		]);