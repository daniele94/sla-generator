'use strict';

angular.module('slaGenerator').controller('AppSetupController',
		['$sessionStorage', '$localStorage', 'AppSetupService', '$scope', '$location', '$state', function( $sessionStorage, $localStorage, AppSetupService, $scope, $location, $state) {

			console.log("AppSetupController called");

			var self = this;

			//Global Variables
			console.log("AppSetupController -> musa jwt token: "+$sessionStorage.musaJwtToken);
			console.log("AppSetupController -> musa user id: "+$sessionStorage.musaUserId);

			//Variables
			self.application = $sessionStorage.application;
			self.selectedApplication = null;
			self.appName = null;
			self.appMacmId = null;

			//Functions
			self.removeAllApps = removeAllApps;
			self.getAllApps = getAllApps;
			self.loadApplicationAndComponent = loadApplicationAndComponent;
			self.createNewApp = createNewApp;
			self.importAppFromMACM = importAppFromMACM;
			self.submitAppMacm = submitAppMacm;

			//Get applications to set dropdown
			getAllApps();

			//Functions for source params management
			if($location.search().appId != null && $location.search().componentId != null){

				AppSetupService.setuppAppFromKanban($location.search().appId, $location.search().componentId, $sessionStorage.musaJwtToken, $sessionStorage.musaUserId)
				.then(function (response) {
					console.log('Component SetUp executed successfully ');

					$sessionStorage.application = {id : null, kanbanId : null, name : null, state : null, allSlatSync : false, slaId : null, 
							activeComponent : 
							{id : null, kanbanId : null, name : null, state : null, riskJson : null, metrics : null, slaId : null, slaXml : null, 
								assessedMetrics : null, assessedSlaId : null, assessedSlaXml : null, composedSlaId : null, composedSlaXml : null, 
								hasQuestions : null, slatSync: null, componentGroup: null, threatQuestions : null, threats : null, controls : null},
								components : []};
					self.application = $sessionStorage.application;

					var appDownloaded = response.data;
					$sessionStorage.application.id = appDownloaded.id;
					$sessionStorage.application.kanbanId = appDownloaded.kanbanId;
					$sessionStorage.application.name = appDownloaded.name;
					$sessionStorage.application.state = appDownloaded.state;

					var componentsDwnld = appDownloaded.components;

					for (var i = 0; i < componentsDwnld.length; i++) { 
						console.log("Self.source: "+self.source);
						console.log("component id: "+$location.search().componentId);
						console.log("component id to check: "+componentsDwnld[i].id);
						console.log("component kanban id to check: "+componentsDwnld[i].kanbanId);
						if(componentsDwnld[i].kanbanId == $location.search().componentId){
							console.log("ACTIVE COMPONENT FOUND!");
							console.log("ITS STATE IS: "+componentsDwnld[i].state);

							$sessionStorage.application.activeComponent.id = componentsDwnld[i].id;
							$sessionStorage.application.activeComponent.kanbanId = componentsDwnld[i].kanbanId;
							$sessionStorage.application.activeComponent.state = componentsDwnld[i].state;
							$sessionStorage.application.activeComponent.name = componentsDwnld[i].name;
							$sessionStorage.application.activeComponent.slaId = componentsDwnld[i].slaId;
							$sessionStorage.application.activeComponent.assessedSlaId = componentsDwnld[i].assessedSlaId;
							$sessionStorage.application.activeComponent.slatSync = componentsDwnld[i].slatSync;
							$sessionStorage.application.activeComponent.componentGroup = componentsDwnld[i].componentGroup;
							$sessionStorage.application.components.push($sessionStorage.application.activeComponent);
							loadSlaXmlForActiveComponent();
						}else{
							console.log("Component group: "+componentsDwnld[i].componentGroup);
							var component = {id : componentsDwnld[i].id, kanbanId : componentsDwnld[i].kanbanId, 
									name : componentsDwnld[i].name, state : componentsDwnld[i].state, riskJson : null, metrics : null, slaId : componentsDwnld[i].slaId, slaXml : null, 
									assessedMetrics : null, assessedSlaId : componentsDwnld[i].assessedSlaId, assessedSlaXml : null, questions : null, slatSync : componentsDwnld[i].slatSync, componentGroup: componentsDwnld[i].componentGroup}
							$sessionStorage.application.components.push(component);
						}
					}

					self.application = $sessionStorage.application;
					if($sessionStorage.application.activeComponent.state == "CREATED"){
						$state.go('appviewer');
					}else if($sessionStorage.application.activeComponent.state == "PROFILED"){
						$state.go('slagenerator');
					}else if($sessionStorage.application.activeComponent.state == "SLAT_GENERATED"){
						$state.go('assessment');
					}else if($sessionStorage.application.activeComponent.state == "SLAT_ASSESSED"){
						$state.go('app_compose');
					}else if($sessionStorage.application.activeComponent.state == "SLA_GENERATED"){
						$state.go('appviewer');
					}else{
						$state.go('appviewer');
					}
					alert('App and Component SetUp executed successfully!');
				},
				function (errResponse) {
					$sessionStorage.application = {id : null, kanbanId : null, name : null, state : null, allSlatSync : false, slaId : null, 
							activeComponent : 
							{id : null, kanbanId : null, name : null, state : null, riskJson : null, metrics : null, slaId : null, slaXml : null, 
								assessedMetrics : null, assessedSlaId : null, assessedSlaXml : null, composedSlaId : null, composedSlaXml : null, 
								hasQuestions : null, slatSync: null, componentGroup: null, threatQuestions : null, threats : null, controls : null},
								components : []};
					self.application = $sessionStorage.application;

					var errMessage = "";
					if(errResponse.data != null){
						errMessage = errResponse.data.message;
					}
					alert("Error while SetUp the component!\n"+errMessage);
					console.error('Error while SetUp the component '+errMessage);
					self.errorMessage = 'Error while SetUp the component: ' + errMessage;
					$state.go('appviewer');
				}
				);
			}

			function removeAllApps() {
				console.log("removeAllApps");
				AppSetupService.removeAllApps()
				.then(function (response) {
					console.log('Apps removed successfully '+response.data);
					alert('Apps removed successfully');
					$state.reload();
				},
				function (errResponse) {
					alert("Error while remove apps");
					console.error('Error while remove apps');
				}
				);
			}

			function getAllApps() {
				console.log("getAllApps");
				AppSetupService.getAllApps()
				.then(function (response) {
					console.log('Apps get successfully ');
					self.applications = response.data;
				},
				function (errResponse) {
					alert("Error while get apps");
				}
				);
			}

			function loadApplicationAndComponent(){
				console.log("loadApplicationAndComponent called;");

				if($scope.selectedApplication != null && $scope.selectedComponent != null){
					console.log("Get app from id called;");
					AppSetupService.getAppFromId($scope.selectedApplication.id)
					.then(function (response) {
						console.log('App get successfully! '+response.data);
						var appDownloaded = response.data;
						$sessionStorage.application.id = appDownloaded.id;
						$sessionStorage.application.kanbanId = appDownloaded.kanbanId;
						$sessionStorage.application.name = appDownloaded.name;
						$sessionStorage.application.state = appDownloaded.state;

						var componentsDwnld = appDownloaded.components;

						for (var i = 0; i < componentsDwnld.length; i++) { 
							if(componentsDwnld[i].kanbanId == $scope.selectedComponent.kanbanId && 
									componentsDwnld[i].id == $scope.selectedComponent.id){
								console.log("ACTIVE COMPONENT FOUND! "+JSON.stringify(componentsDwnld[i]));
								$sessionStorage.application.activeComponent.id = componentsDwnld[i].id;
								$sessionStorage.application.activeComponent.kanbanId = componentsDwnld[i].kanbanId;
								$sessionStorage.application.activeComponent.state = componentsDwnld[i].state;
								$sessionStorage.application.activeComponent.name = componentsDwnld[i].name;
								$sessionStorage.application.activeComponent.slaId = componentsDwnld[i].slaId;
								$sessionStorage.application.activeComponent.assessedSlaId = componentsDwnld[i].assessedSlaId;
								$sessionStorage.application.activeComponent.slatSync = componentsDwnld[i].slatSync;
								$sessionStorage.application.activeComponent.componentGroup = componentsDwnld[i].componentGroup;
								$sessionStorage.application.activeComponent.composedSlaId = componentsDwnld[i].comosedSlaId;
								$sessionStorage.application.components.push($sessionStorage.application.activeComponent);
								loadSlaXmlForActiveComponent();
							}else{
								var component = {id : componentsDwnld[i].id, 
										kanbanId : componentsDwnld[i].kanbanId,
										name : componentsDwnld[i].name, 
										state : componentsDwnld[i].state, 
										riskJson : null, 
										metrics : null, 
										slaId : componentsDwnld[i].slaId, 
										slaXml : null, 
										assessedMetrics : null, 
										assessedSlaId : componentsDwnld[i].assessedSlaId, 
										assessedSlaXml : null, 
										composedSlaId : componentsDwnld[i].comosedSlaId, 
										composedSlaXml : null,
										questions : null, 
										slatSync : componentsDwnld[i].slatSync, 
										componentGroup : componentsDwnld[i].componentGroup}
								$sessionStorage.application.components.push(component);
							}
						}

						self.application = $sessionStorage.application;
						$state.go('appviewer');
						alert('App and Component SetUp executed successfully!');
					},
					function (errResponse) {
						alert("Error while get Application!");
					}
					);
				}else{
					console.log('Component SetUp failed');
					alert('App and Component SetUp failed!');
				}
			}

			function loadSlaXmlForActiveComponent(){
				if($sessionStorage.application.activeComponent.slaId != null){
					AppSetupService.getSlaXml($sessionStorage.application.activeComponent.slaId)
					.then(function (response) {
						console.log('Sla Content Get successfully');
						if(response.data != null){
							$sessionStorage.application.activeComponent.slaXml = response.data.replace(/<(\/?)([^:>\s]*:)?([^>]+)>/g, "<$1$3>");
							self.application = $sessionStorage.application;
						}else{
							console.log('Response from sla repo is null!');
						}
					},
					function (errResponse) {
						console.log("Error while get sla xml");
					}
					);
				}else{
					console.log('No sla found on component!');
				}

				if($sessionStorage.application.activeComponent.assessedSlaId != null){
					AppSetupService.getSlaXml($sessionStorage.application.activeComponent.assessedSlaId)
					.then(function (response) {
						console.log('Sla Content Get successfully');
						if(response.data != null){
							$sessionStorage.application.activeComponent.assessedSlaXml = response.data.replace(/<(\/?)([^:>\s]*:)?([^>]+)>/g, "<$1$3>");
							console.log("self.slaFromRepoXml "+$sessionStorage.application.activeComponent.assessedSlaXml);
							self.application = $sessionStorage.application;
						}else{
							console.log('Response from sla repo is null!');
						}
					},
					function (errResponse) {
						console.log("Error while get sla xml");
					}
					);
				}else{
					console.log('No assessed sla found on component!');
				}
			}

			function createNewApp() {
				console.log("createNewApp");
				AppSetupService.createNewApp(self.appName)
				.then(function (response) {
					var appCreated = response.data;
					self.application = {id : null, kanbanId : null, name : null, state : null, allSlatSync : false, slaId : null, 
							activeComponent : 
							{id : null, kanbanId : null, name : null, state : null, riskJson : null, metrics : null, slaId : null, slaXml : null, 
								assessedMetrics : null, assessedSlaId : null, assessedSlaXml : null, questions : null, slatSync: null, componentGroup: null},
								components : []};
					self.application.id = appCreated.id;
					self.application.name = appCreated.name;
					self.application.state = appCreated.state;
					self.application.kanbanId = appCreated.kanbanId;
					$sessionStorage.application = self.application;

					//TODO: andare in risk analysis
					$state.go('appviewer');
					console.log('New App created successfully '+JSON.stringify(self.application));
					alert('New Application created successfully!');
				},
				function (errResponse) {
					alert("Error while Create new App\n"+errResponse.data.message);
					console.error('Error while Create new App '+errResponse.data.message);
					self.errorMessage = 'Error while Create new App: ' + errResponse.data.message;
				}
				);
			}

			function importAppFromMACM() {
				console.log("importAppFromMACM");
				if(self.appMacmId != null){
					AppSetupService.importAppFromMACM(self.appMacmId)
					.then(function (response) {
						var appCreated = response.data;
						console.log("appCreated: "+JSON.stringify(appCreated));

						self.application = {id : null, kanbanId : null, name : null, state : null, allSlatSync : false, slaId : null, 
								activeComponent : 
								{id : null, kanbanId : null, name : null, state : null, riskJson : null, metrics : null, slaId : null, slaXml : null, 
									assessedMetrics : null, assessedSlaId : null, assessedSlaXml : null, questions : null, slatSync: null, componentGroup: null},
									components : []};
						self.application.id = appCreated.id;
						self.application.name = appCreated.name;
						self.application.state = appCreated.state;
						self.application.kanbanId = appCreated.kanbanId;

						var componentsDwnld = appCreated.components;
						console.log("componentsDwnld: "+JSON.stringify(componentsDwnld));
						for (var i = 0; i < componentsDwnld.length; i++) { 
							if(i == 0){
								self.application.activeComponent.id = componentsDwnld[i].id;
								self.application.activeComponent.kanbanId = componentsDwnld[i].kanbanId;
								self.application.activeComponent.state = componentsDwnld[i].state;
								self.application.activeComponent.name = componentsDwnld[i].name;
								self.application.activeComponent.slaId = componentsDwnld[i].slaId;
								self.application.activeComponent.assessedSlaId = componentsDwnld[i].assessedSlaId;
								self.application.activeComponent.slatSync = componentsDwnld[i].slatSync;
								self.application.activeComponent.componentGroup = componentsDwnld[i].componentGroup;
								self.application.components.push(self.application.activeComponent);
								loadSlaXmlForActiveComponent();
							}else{
								var component = {id : componentsDwnld[i].id, kanbanId : componentsDwnld[i].kanbanId, 
										name : componentsDwnld[i].name, state : componentsDwnld[i].state, riskJson : null, metrics : null, slaId : componentsDwnld[i].slaId, slaXml : null, 
										assessedMetrics : null, assessedSlaId : componentsDwnld[i].assessedSlaId, assessedSlaXml : null, questions : null, slatSync : componentsDwnld[i].slatSync, componentGroup : componentsDwnld[i].componentGroup}
								self.application.components.push(component);
							}
						}

						$sessionStorage.application = self.application;

						$state.go('appviewer');
						console.log('New App created successfully '+JSON.stringify(self.application));
						alert('New Application created successfully!');
					},
					function (errResponse) {
						alert("Error while import new App\n"+errResponse.data.message);
						console.error('Error while import App '+errResponse.data.message);
						self.errorMessage = 'Error while import new App: ' + errResponse.data.message;
					}
					);
				}else{
					alert("Error! Please insert an app id!");
				}
			}



			//Functions for json file upload
			document.getElementById('input-app-macm')
			.addEventListener('change',
					readAppMacmFile, false);

			function readAppMacmFile(e) {
				var file = e.target.files[0];
				if (!file) {
					return;
				}
				var reader = new FileReader();

				reader.readAsText(file);

				reader.onload = function(e) {
					var contents = e.target.result;
					$scope.appMacmContent = contents;
					$scope.$apply();
				};

			}

			function submitAppMacm() {
				console.log("submitAppMacm");
				AppSetupService.submitAppMacm($scope.appMacmContent, self.appMacmId)
				.then(function (response) {
					var appCreated = response.data;
					console.log("appCreated: "+JSON.stringify(appCreated));

					self.application = {id : null, kanbanId : null, name : null, state : null, allSlatSync : false, slaId : null, 
							activeComponent : 
							{id : null, kanbanId : null, name : null, state : null, riskJson : null, metrics : null, slaId : null, slaXml : null, 
								assessedMetrics : null, assessedSlaId : null, assessedSlaXml : null, questions : null, slatSync: null, componentGroup: null},
								components : []};
					self.application.id = appCreated.id;
					self.application.name = appCreated.name;
					self.application.state = appCreated.state;
					self.application.kanbanId = appCreated.kanbanId;

					var componentsDwnld = appCreated.components;
					console.log("componentsDwnld: "+JSON.stringify(componentsDwnld));
					for (var i = 0; i < componentsDwnld.length; i++) { 
						if(i == 0){
							self.application.activeComponent.id = componentsDwnld[i].id;
							self.application.activeComponent.kanbanId = componentsDwnld[i].kanbanId;
							self.application.activeComponent.state = componentsDwnld[i].state;
							self.application.activeComponent.name = componentsDwnld[i].name;
							self.application.activeComponent.slaId = componentsDwnld[i].slaId;
							self.application.activeComponent.assessedSlaId = componentsDwnld[i].assessedSlaId;
							self.application.activeComponent.slatSync = componentsDwnld[i].slatSync;
							self.application.activeComponent.componentGroup = componentsDwnld[i].componentGroup;
							self.application.components.push(self.application.activeComponent);
							loadSlaXmlForActiveComponent();
						}else{
							var component = {id : componentsDwnld[i].id, kanbanId : componentsDwnld[i].kanbanId, 
									name : componentsDwnld[i].name, state : componentsDwnld[i].state, riskJson : null, metrics : null, slaId : componentsDwnld[i].slaId, slaXml : null, 
									assessedMetrics : null, assessedSlaId : componentsDwnld[i].assessedSlaId, assessedSlaXml : null, questions : null, slatSync : componentsDwnld[i].slatSync, componentGroup : componentsDwnld[i].componentGroup}
							self.application.components.push(component);
						}
					}

					$sessionStorage.application = self.application;

					$state.go('appviewer');
					console.log('New App created successfully '+JSON.stringify(self.application));
					alert('New Application created successfully!');
				},
				function (errResponse) {
					alert("Error while import new App\n"+errResponse.data.message);
					console.error('Error while import App '+errResponse.data.message);
					self.errorMessage = 'Error while import new App: ' + errResponse.data.message;
				}
				);
			}

		}]);