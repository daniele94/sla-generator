'use strict';



angular.module('slaGenerator').controller('StrideImpactEvaluationController',
		['$sessionStorage', 'StrideImpactEvaluationService', '$scope', '$location', '$state', '$window',
			function( $sessionStorage, StrideImpactEvaluationService, $scope, $location, $state, $window) {

			console.log("StrideImpactEvaluationController called");

			var self = this;

			//Variables
			self.application = $sessionStorage.application;


			//Functions
			self.setDefaultImpactStrideValues = setDefaultImpactStrideValues;
			self.setDefaultImpactStrideValues();
			self.saveImpactEvaluation = saveImpactEvaluation;





				function setDefaultImpactStrideValues()
				{
					console.log("Default values Called");
					try {
						self.application.impact.spoofing.financialdamage = 1;
						self.application.impact.spoofing.reputationdamage = 1;
						self.application.impact.spoofing.noncompliance = 2;
						self.application.impact.spoofing.privacyviolation = 3;

						self.application.impact.tampering.financialdamage = 1;
						self.application.impact.tampering.reputationdamage = 1;
						self.application.impact.tampering.noncompliance = 2;
						self.application.impact.tampering.privacyviolation = 3;

						self.application.impact.Reputation.financialdamage = 1;
						self.application.impact.Reputation.reputationdamage = 1;
						self.application.impact.Reputation.noncompliance = 2;
						self.application.impact.Reputation.privacyviolation = 3;

						self.application.impact.informationDisclosure.financialdamage = 1;
						self.application.impact.informationDisclosure.reputationdamage = 1;
						self.application.impact.informationDisclosure.noncompliance = 2;
						self.application.impact.informationDisclosure.privacyviolation = 3;

						self.application.impact.dos.financialdamage = 1;
						self.application.impact.dos.reputationdamage = 1;
						self.application.impact.dos.noncompliance = 2;
						self.application.impact.dos.privacyviolation = 3;

						self.application.impact.elevationOfPrivileges.financialdamage = 1;
						self.application.impact.elevationOfPrivileges.reputationdamage = 1;
						self.application.impact.elevationOfPrivileges.noncompliance = 2;
						self.application.impact.elevationOfPrivileges.privacyviolation = 3;
					}
					catch(err)
					{
					}

				};

				function saveImpactEvaluation()
				{
					console.log(self.application.impact);


					return StrideImpactEvaluationService.saveImpactEvaluation(self.application.id,
						self.application.impact)
						.then(function (response) {
								console.log('Impact evaluate successfully '+JSON.stringify(response));
								alert("Impact evaluate successfully!\n");
							},
							function (errResponse) {
								console.error('Error while Impact evaluate ' + errResponse.data.message);
								self.errorMessage = 'Error while Impact evaluate: ' + errResponse.data.message;
								alert('Error while Impact evaluate!\n' + errResponse.data.message);
							}
						);

				}







		}]);