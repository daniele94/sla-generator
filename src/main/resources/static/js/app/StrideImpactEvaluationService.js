'use strict';

angular.module('slaGenerator').factory('StrideImpactEvaluationService',
		['$sessionStorage','$localStorage', '$http', '$q', 'urls',
			function ($sessionStorage, $localStorage, $http, $q, urls) {

			var factory = {
				saveImpactEvaluation:saveImpactEvaluation
			};

			return factory;


				function saveImpactEvaluation(appId,impactScores){
					var deferred = $q.defer();

					$http(
						{
							method : 'POST',
							url : urls.RISKANALYSIS+"/saveImpactEvaluation",
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							transformRequest : function(
								obj) {
								var str = [];
								for ( var p in obj)
									str
										.push(encodeURIComponent(p)
											+ "="
											+ encodeURIComponent(obj[p]));
								return str.join("&");
							},
							data : {
								'appId' : appId,
								'impactScores':JSON.stringify(impactScores)

							}

						})
						.then(
							function successCallback(
								response) {
								deferred.resolve(response.data);
							},
							function errorCallback(
								errResponse) {
								deferred.reject(errResponse);
							});

					return deferred.promise;
				}

			




		}
		]);