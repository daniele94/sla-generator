
<style>
    a.tip {
        text-decoration: none
    }

    a.tip:hover {
        cursor: help;
        position: relative
    }

    a.tip span {
        display: none
    }

    a.tip:hover span {
        border: #c0c0c0 1px dotted;
        padding: 5px 20px 5px 5px;
        display: block;
        z-index: 100;
        background: #f0f0f0 no-repeat 100% 5%;
        left: 0px;
        margin: 10px;
        width: 250px;
        position: absolute;
        top: 10px;
        text-decoration: none
    }






</style>

<style>
    table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    td, th {
        text-align: left;
        padding: 8px;
    }


</style>

<div class=container>
    <div class="panel panel-default">
        <div class="panel-heading text-center">
            <span class="lead">
            <h2>Threat Agent Wizard</h2>
                </span>
        </div>
        <br>



        <div class="panel panel-default">
            <div class="panel-body">

                <table border="1" style="width: 99%; ">


                    <tr style="border-bottom: 1px solid #ddd" ng-repeat="TAQuestion in ctrl.TAQuestions">

                        <td style="padding: 10px;" width="5%"><b>Question: {{TAQuestion.questionId}}</b></td>
                        <td style="padding: 10px;" width="55%"><br><b>{{TAQuestion.question}}</b>

                            <br>

                        <table  style="width: 100%;">
                            <tr ng-repeat="TAReply in ctrl.TAReplies">
                                <br>
                                <td
                                 ng-show="TAQuestion.questionId == TAReply.question.questionId">

                                    <input type="checkbox" ng-click="manageCheckbox(TAReply)" ng-model="TAReply.selected">

                                    <a class="tip">{{TAReply.reply}}<span>{{TAReply.description}}</span></a>
                                    </input>


                                </td>


                            </tr>
                        </table>

                        </td>


                    </tr>
                </table>

            </div>
        </div>

        <div style="text-align: center;">
            <button style="text-align: center;" class="btn btn-primary"
                    ng-click="ctrl.submitQuestions()">Submit Questions</button>
        </div>
        <br>
        <br>
        <div class="panel-heading text-center">
            <span class="lead">
            <h2>Categories Result</h2>
                </span>
        </div>

        <table border="1" style="width: 100%;" ng-show="show_table">

            <tr style="border-bottom: 1px solid #ddd" ng-repeat="Category in ctrl.Categories">
                <br>
                <td style="padding: 4px;" width="30%">
                    <h4><b>Category</b></h4>
                    <h3> {{Category.threatAgentCategory.name}} </h3>
                </td>

                <td  style="padding: 4px;" width="45%">
                    <h4><b>Description</b></h4>
                     {{Category.threatAgentCategory.description}}
                    <h4><b>Threat Agent Common Actions</b></h4>
                       {{Category.threatAgentCategory.actions}}
                        <br>
                    <ul ng-repeat="(key,value) in ctrl.Attributes">
                        <li ng-show="key==Category.threatAgentCategory.name" ng-repeat="v in value">
                             {{v.attribute}}: {{v.scoreLabel}}
                        </li>


                    </ul>

                </td>

                <td width="25%">
                    How does you rate the category?
                    <br>
                    <br>
                    <div class="btn-group btn-toggle">

                        <div class="btn-group" role="group" aria-label="...">

                            <button
                                    ng-class="{'true':'btn btn-primary active','false':'btn btn-default'}[Category.selected == 'Low']"
                                    ng-click="Category.selected = 'Low'">LOW</button>
                            <button
                                    ng-class="{'true':'btn btn-primary active','false':'btn btn-default'}[Category.selected == 'Medium']"
                                    ng-click="Category.selected = 'Medium'">MEDIUM</button>
                            <button
                                    ng-class="{'true':'btn btn-primary active','false':'btn btn-default'}[Category.selected == 'High']"
                                    ng-click="Category.selected = 'High'">HIGH</button>
                        </div>


                    </div>
                </td>

            </tr>


        </table>



    </div>

    <div ng-show="show_table" style="text-align: center;">
        <button style="text-align: center;" class="btn btn-primary"
                ng-click="ctrl.submitEvaluation()">Submit Evaluation</button>
    </div>

</div>