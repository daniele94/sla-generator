
<style>
    a.tip {
        text-decoration: none
    }

    a.tip:hover {
        cursor: help;
        position: relative
    }

    a.tip span {
        display: none
    }

    a.tip:hover span {
        border: #c0c0c0 1px dotted;
        padding: 5px 20px 5px 5px;
        display: block;
        z-index: 100;
        background: #f0f0f0 no-repeat 100% 5%;
        left: 0px;
        margin: 10px;
        width: 250px;
        position: absolute;
        top: 10px;
        text-decoration: none
    }


</style>

<style>
    table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    td, th {
        text-align: left;
        padding: 8px;
    }


</style>

<div class=container>
    <!-- IMPACT DEFAULT EVALUATION-->
    <div class="panel panel-default">
        <div>
            <table border="1" style="width: 100%">
                <tr style="border-bottom: 1px solid #ddd">

                    <td style="padding: 10px;" width="25%">

                        <div style="padding: 3px;">
                            <b>Stride: </b>
                            <p>SPOOFING</p>
                            <b>Description: </b>
                            <p>Threats related to the interception of data in transit</p>

                        </div>

                    </td>
                    <td style="padding: 10px;" width="75%"><div
                                style="padding: 3px; font-size: 120%;">
                            <p>How much impact does a Spoofing threat have on application?</p>
                            <br>
                            <table border="1" style="width: 100%">
                                <tr style="border-bottom: 1px solid #ddd">
                                    <td><a class="tip">Financial damage<span>How
															much financial damage will result from an exploit? Less
															than the cost to fix the vulnerability (1), minor effect
															on annual profit (3), significant effect on annual profit
															(7), bankruptcy (9)</span></a></td>
                                    <td><input type="number"
                                               ng-model="ctrl.application.impact.spoofing.financialdamage" min="0"
                                               max="9" value="1" ></td>
                                </tr>




                                <tr style="border-bottom: 1px solid #ddd">


                                    <td><a class="tip">Reputation damage<span>Would
															an exploit result in reputation damage that would harm
															the business? Minimal damage (1), Loss of major accounts
															(4), loss of goodwill (5), brand damage (9)</span></a></td>

                                    <td><input type="number"
                                               ng-model="ctrl.application.impact.spoofing.reputationdamage"
                                               min="0" max="9" value="1" ></td>
                                </tr>

                                <tr style="border-bottom: 1px solid #ddd">


                                    <td><a class="tip">Non-compliance<span>How
															much exposure does non-compliance introduce? Minor
															violation (2), clear violation (5), high profile
															violation (7)</span></a></td>

                                    <td><input type="number"
                                               ng-model="ctrl.application.impact.spoofing.noncompliance" min="0"
                                               max="9"value="2"  ></td>

                                <tr>
                                    <td><a class="tip">Privacy violation<span>How
															much personally identifiable information could be
															disclosed? One individual (3), hundreds of people (5),
															thousands of people (7), millions of people (9)</span></a></td>

                                    <td><input type="number"
                                               ng-model="ctrl.application.impact.spoofing.privacyviolation" min="0"
                                               max="9" value="3" ></td>
                                </tr>
                                </tr>
                                </tr>



                            </table>

                        </div>
                    </td>
                </tr>


                <tr style="border-bottom: 1px solid #ddd">

                    <td style="padding: 10px;" width="25%">

                        <div style="padding: 3px;">
                            <b>Stride: </b>
                            <p>TAMPERING</p>

                            <b>Description: </b>
                            <p>Threats related to data manipulation</p>

                        </div>

                    </td>
                    <td style="padding: 10px;" width="75%"><div
                                style="padding: 3px; font-size: 120%;">
                            <p>How much impact does a Tampering threat have on application?</p>
                            <br>
                            <table border="1" style="width: 100%">
                                <tr style="border-bottom: 1px solid #ddd">
                                    <td><a class="tip">Financial damage<span>How
															much financial damage will result from an exploit? Less
															than the cost to fix the vulnerability (1), minor effect
															on annual profit (3), significant effect on annual profit
															(7), bankruptcy (9)</span></a></td>
                                    <td><input type="number"
                                               ng-model="ctrl.application.impact.tampering.financialdamage"
                                               min="0" max="9" value="1" ></td>
                                </tr>




                                <tr style="border-bottom: 1px solid #ddd">


                                    <td><a class="tip">Reputation damage<span>Would
															an exploit result in reputation damage that would harm
															the business? Minimal damage (1), Loss of major accounts
															(4), loss of goodwill (5), brand damage (9)</span></a></td>

                                    <td><input type="number"
                                               ng-model="ctrl.application.impact.tampering.reputationdamage"
                                               min="0" max="9" value="1" ></td>
                                </tr>

                                <tr style="border-bottom: 1px solid #ddd">


                                    <td><a class="tip">Non-compliance<span>How
															much exposure does non-compliance introduce? Minor
															violation (2), clear violation (5), high profile
															violation (7)</span></a></td>

                                    <td><input type="number"
                                               ng-model="ctrl.application.impact.tampering.noncompliance" min="0"
                                               max="9" value="2" ></td>

                                <tr>
                                    <td><a class="tip">Privacy violation<span>How
															much personally identifiable information could be
															disclosed? One individual (3), hundreds of people (5),
															thousands of people (7), millions of people (9)</span></a></td>

                                    <td><input type="number"
                                               ng-model="ctrl.application.impact.tampering.privacyviolation" min="0"
                                               max="9" value="3"></td>
                                </tr>
                                </tr>
                                </tr>



                            </table>

                        </div>
                    </td>
                </tr>

                <tr style="border-bottom: 1px solid #ddd">

                    <td style="padding: 10px;" width="25%">

                        <div style="padding: 3px;">
                            <b>Stride: </b>
                            <p>REPUTATION</p>


                            <b>Description: </b>
                            <p>Threats that impact reputation</p>

                        </div>

                    </td>
                    <td style="padding: 10px;" width="75%"><div
                                style="padding: 3px; font-size: 120%;">
                            <p>How much impact does a Reputation threat have on application?</p>
                            <table border="1" style="width: 100%">
                                <tr style="border-bottom: 1px solid #ddd">
                                    <td><a class="tip">Financial damage<span>How
															much financial damage will result from an exploit? Less
															than the cost to fix the vulnerability (1), minor effect
															on annual profit (3), significant effect on annual profit
															(7), bankruptcy (9)</span></a></td>
                                    <td><input type="number"
                                               ng-model="ctrl.application.impact.Reputation.financialdamage"
                                               min="0" max="9" value="1"></td>
                                </tr>




                                <tr style="border-bottom: 1px solid #ddd">


                                    <td><a class="tip">Reputation damage<span>Would
															an exploit result in reputation damage that would harm
															the business? Minimal damage (1), Loss of major accounts
															(4), loss of goodwill (5), brand damage (9)</span></a></td>

                                    <td><input type="number"
                                               ng-model="ctrl.application.impact.Reputation.reputationdamage"
                                               min="0" max="9" value="1"></td>
                                </tr>

                                <tr style="border-bottom: 1px solid #ddd">


                                    <td><a class="tip">Non-compliance<span>How
															much exposure does non-compliance introduce? Minor
															violation (2), clear violation (5), high profile
															violation (7)</span></a></td>

                                    <td><input type="number"
                                               ng-model="ctrl.application.impact.Reputation.noncompliance" min="0"
                                               max="9" value="2"></td>

                                <tr>
                                    <td><a class="tip">Privacy violation<span>How
															much personally identifiable information could be
															disclosed? One individual (3), hundreds of people (5),
															thousands of people (7), millions of people (9)</span></a></td>

                                    <td><input type="number"
                                               ng-model="ctrl.application.impact.Reputation.privacyviolation"
                                               min="0" max="9" value="3" ></td>
                                </tr>
                                </tr>
                                </tr>

                            </table>
                        </div>
                    </td>
                </tr>

                <tr style="border-bottom: 1px solid #ddd">

                    <td style="padding: 10px;" width="25%">

                        <div style="padding: 3px;">
                            <b>Stride: </b>
                            <p>INFORMATION DISCLOSURE</p>
                            <b>Description: </b>
                            <p>Threats related to access to confidential information</p>

                        </div>

                    </td>
                    <td style="padding: 10px;" width="75%"><div
                                style="padding: 3px; font-size: 120%;">
                            <p>How much impact does a Information Disclosure threat have on application?</p>
                            <table border="1" style="width: 100%">
                                <tr style="border-bottom: 1px solid #ddd">
                                    <td><a class="tip">Financial damage<span>How
															much financial damage will result from an exploit? Less
															than the cost to fix the vulnerability (1), minor effect
															on annual profit (3), significant effect on annual profit
															(7), bankruptcy (9)</span></a></td>
                                    <td><input type="number"
                                               ng-model="ctrl.application.impact.informationDisclosure.financialdamage"
                                               min="0" max="9" value="1"></td>
                                </tr>




                                <tr style="border-bottom: 1px solid #ddd">


                                    <td><a class="tip">Reputation damage<span>Would
															an exploit result in reputation damage that would harm
															the business? Minimal damage (1), Loss of major accounts
															(4), loss of goodwill (5), brand damage (9)</span></a></td>

                                    <td><input type="number"
                                               ng-model="ctrl.application.impact.informationDisclosure.reputationdamage"
                                               min="0" max="9" value="1" ></td>
                                </tr>

                                <tr style="border-bottom: 1px solid #ddd">


                                    <td><a class="tip">Non-compliance<span>How
															much exposure does non-compliance introduce? Minor
															violation (2), clear violation (5), high profile
															violation (7)</span></a></td>

                                    <td><input type="number"
                                               ng-model="ctrl.application.impact.informationDisclosure.noncompliance"
                                               min="0" max="9" value="2"></td>

                                <tr>
                                    <td><a class="tip">Privacy violation<span>How
															much personally identifiable information could be
															disclosed? One individual (3), hundreds of people (5),
															thousands of people (7), millions of people (9)</span></a></td>

                                    <td><input type="number"
                                               ng-model="ctrl.application.impact.informationDisclosure.privacyviolation" min="0"
                                               max="9" value="3"></td>
                                </tr>
                                </tr>
                                </tr>



                            </table>
                        </div>
                    </td>
                </tr>

                <tr style="border-bottom: 1px solid #ddd">

                    <td style="padding: 10px;" width="25%">

                        <div style="padding: 3px;">
                            <b>Stride: </b>
                            <p>DENIAL OF SERVICES</p>
                            <b>Description: </b>
                            <p>Attacks on availability</p>



                        </div>

                    </td>
                    <td style="padding: 10px;" width="75%"><div
                                style="padding: 3px; font-size: 120%;">

                            <p>How much impact does a Denial of services threat have on application?</p>
                            <table border="1" style="width: 100%">
                                <tr style="border-bottom: 1px solid #ddd">
                                    <td><a class="tip">Financial damage<span>How
															much financial damage will result from an exploit? Less
															than the cost to fix the vulnerability (1), minor effect
															on annual profit (3), significant effect on annual profit
															(7), bankruptcy (9)</span></a></td>
                                    <td><input type="number"
                                               ng-model="ctrl.application.impact.dos.financialdamage" min="0"
                                               max="9"value="1" ></td>
                                </tr>




                                <tr style="border-bottom: 1px solid #ddd">


                                    <td><a class="tip">Reputation damage<span>Would
															an exploit result in reputation damage that would harm
															the business? Minimal damage (1), Loss of major accounts
															(4), loss of goodwill (5), brand damage (9)</span></a></td>

                                    <td><input type="number"
                                               ng-model="ctrl.application.impact.dos.reputationdamage" min="0" max="9"
                                               value="1"></td>
                                </tr>

                                <tr style="border-bottom: 1px solid #ddd">


                                    <td><a class="tip">Non-compliance<span>How
															much exposure does non-compliance introduce? Minor
															violation (2), clear violation (5), high profile
															violation (7)</span></a></td>

                                    <td><input type="number"
                                               ng-model="ctrl.application.impact.dos.noncompliance" min="0"
                                               max="9" value="2"></td>

                                <tr>
                                    <td><a class="tip">Privacy violation<span>How
															much personally identifiable information could be
															disclosed? One individual (3), hundreds of people (5),
															thousands of people (7), millions of people (9)</span></a></td>

                                    <td><input type="number"
                                               ng-model="ctrl.application.impact.dos.privacyviolation" min="0"
                                               max="9" value="3"></td>
                                </tr>
                                </tr>
                                </tr>



                            </table>
                        </div>
                    </td>
                </tr>

                <tr style="border-bottom: 1px solid #ddd">

                    <td style="padding: 10px;" width="25%">

                        <div style="padding: 3px;">
                            <b>Stride: </b>
                            <p>ELEVATION OF PRIVILEGES</p>
                            <b>Description: </b>
                            <p>Access to resources with privileges other than their own</p>


                        </div>

                    </td>
                    <td style="padding: 10px;" width="75%"><div
                                style="padding: 3px; font-size: 120%;">
                            <p>How much impact does a Elevation of privileges threat have on application?</p>
                            <table border="1" style="width: 100%">
                                <tr style="border-bottom: 1px solid #ddd">
                                    <td><a class="tip">Financial damage<span>How
															much financial damage will result from an exploit? Less
															than the cost to fix the vulnerability (1), minor effect
															on annual profit (3), significant effect on annual profit
															(7), bankruptcy (9)</span></a></td>
                                    <td><input type="number"
                                               ng-model="ctrl.application.impact.elevationOfPrivileges.financialdamage"
                                               min="0" max="9" value="1"></td>
                                </tr>




                                <tr style="border-bottom: 1px solid #ddd ">


                                    <td><a class="tip">Reputation damage<span>Would
															an exploit result in reputation damage that would harm
															the business? Minimal damage (1), Loss of major accounts
															(4), loss of goodwill (5), brand damage (9)</span></a></td>

                                    <td><input type="number"
                                               ng-model="ctrl.application.impact.elevationOfPrivileges.reputationdamage"
                                               min="0" max="9" value="1"></td>
                                </tr>

                                <tr style="border-bottom: 1px solid #ddd">


                                    <td><a class="tip">Non-compliance<span>How
															much exposure does non-compliance introduce? Minor
															violation (2), clear violation (5), high profile
															violation (7)</span></a></td>

                                    <td><input type="number"
                                               ng-model="ctrl.application.impact.elevationOfPrivileges.noncompliance" min="0"
                                               max="9" value="2"></td>

                                <tr>
                                    <td><a class="tip">Privacy violation<span>How
															much personally identifiable information could be
															disclosed? One individual (3), hundreds of people (5),
															thousands of people (7), millions of people (9)</span></a></td>

                                    <td><input type="number"
                                               ng-model="ctrl.application.impact.elevationOfPrivileges.privacyviolation" min="0"
                                               max="9" value="3"></td>
                                </tr>
                                </tr>
                                </tr>



                            </table>
                        </div>
                    </td>
                </tr>
            </table>

        </div>



    </div>

    <div style="text-align: center;">
        <button style="text-align: center;" class="btn btn-primary"
                ng-click="ctrl.saveImpactEvaluation()">Save Impact Evaluation</button>
    </div>

</div>