
<div class="jumbotron">
	<div class="container">
		<h1>MUSA SLA Generator</h1>
		<p>Welcome to the MUSA SLA Generator Tool.</p>
		<br />

		<p>This tool is useful to manage all the phases for creating a SLA
			related to a multi-cloud application.</p>
		<br>
		<p>In particular you can:</p>
		<li><a ui-sref="slagenerator">Generate a SLA for each of your application components from the risk
				analysis results</a></li>
		<li><a ui-sref="assessment">Execute the SLA assessment</a></li>
		<li><a ui-sref="slaviewer">View SLAs in pretty format</a></li> <br>
		<p>First of all you have to set-up the application.</p>
		<li><a ui-sref="app_setup">SetUp Application</a></li>
	</div>
</div>
