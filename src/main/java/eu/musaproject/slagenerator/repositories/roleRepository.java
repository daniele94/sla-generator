package eu.musaproject.slagenerator.repositories;

import eu.musaproject.model.*;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface roleRepository extends JpaRepository<Role, Long> {


     Role findRoleByRoleAndProtocol(String role, Protocol p);
}
