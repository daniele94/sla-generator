package eu.musaproject.slagenerator.repositories;

import eu.musaproject.model.Application;
import eu.musaproject.model.Component;
import eu.musaproject.model.Relation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface RelationRepository extends JpaRepository<Relation, Long> {

    @Transactional
    @Modifying
    @Query("delete from Relation r where r.application= ?1")
    void deleteByApp(Application application);

    @Query(value ="SELECT * FROM Relation r where r.component_id=?1",nativeQuery = true)
    List<Relation> findByComponent(Long compId);
}
