package eu.musaproject.slagenerator.repositories;

import eu.musaproject.model.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;

public interface QuestionThreatAgentOnApplicationRepository extends JpaRepository<Question_Threat_Agent_On_Application, Long> {

    @Modifying
    @Transactional
    @Query("delete from Question_Threat_Agent_On_Application qtaoa where qtaoa.application.id= ?1")
    void deleteByAppId(Long appId);



}
