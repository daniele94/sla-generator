package eu.musaproject.slagenerator.repositories;

import eu.musaproject.model.Replies;
import eu.musaproject.model.ReplyCategory;
import eu.musaproject.model.ThreatAgentCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ReplyCategoryRepository extends JpaRepository<ReplyCategory, Long> {


    //@Query("select c from Control c join c.metrics m where (m.metricId= :metricId)")

    @Query("SELECT r FROM ReplyCategory r join r.reply m WHERE (m.reply=?1)")
    List<ReplyCategory> getCategories(String replyString);


}
