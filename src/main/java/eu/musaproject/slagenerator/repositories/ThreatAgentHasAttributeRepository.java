package eu.musaproject.slagenerator.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import eu.musaproject.model.*;
import org.springframework.data.jpa.repository.Query;

import java.util.List;


public interface ThreatAgentHasAttributeRepository extends JpaRepository<Ta_has_attribute, Long>
{

    @Query(value = "SELECT * FROM ta_has_attribute WHERE ta_has_attribute.ta_attribute=attribute", nativeQuery = true )
    List<ThreatAgentCategory> findBytaAttribute(ThreatAgentAttribute attribute);


}
