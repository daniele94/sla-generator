package eu.musaproject.slagenerator.repositories;

import eu.musaproject.model.Stride;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StrideRepository extends JpaRepository<Stride, Long> {
	
	Stride findByName(String name);

}
