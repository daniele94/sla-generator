package eu.musaproject.slagenerator.repositories;

import eu.musaproject.model.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;


public interface ta_category_applies_on_application_Repository extends JpaRepository<ta_category_applies_on_application, Long> {

    /*
    @Query("SELECT r FROM ReplyCategory r join r.reply m WHERE (m.reply=?1)")
    List<ReplyCategory> getCategories(String replyString);

     */

    @Query("select ta from ta_category_applies_on_application ta  where (ta.application.id= ?1)")
    List<ta_category_applies_on_application> getCategories(Long appId);

    @Modifying
    @Transactional
    @Query("delete from ta_category_applies_on_application tcaoa where tcaoa.application.id= ?1")
    void deleteByAppId(Long appId);


    @Modifying
    @Transactional
    @Query("update ta_category_applies_on_application u set u.motive_score = ?1, u.opportunity_score = ?2,u.size_score=?3,u.skill_level_score=?4 where u.ta.id = ?5")
    void update(double motive_score,double opportunity_score,double size_score,double skill_level_score,Long tacId);

    @Modifying
    @Transactional
    @Query("update ta_category_applies_on_application u set u.category_weight = ?1 where u.ta=?2")
    void updateScore(String score,ThreatAgentCategory tac);


    ta_category_applies_on_application findByta(ThreatAgentCategory key);
}
