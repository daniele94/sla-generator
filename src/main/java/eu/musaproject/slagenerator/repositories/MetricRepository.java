package eu.musaproject.slagenerator.repositories;

import eu.musaproject.model.Metric;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MetricRepository extends JpaRepository<Metric, Long> {
	
	Metric findByMetricId(String metricId);
}
