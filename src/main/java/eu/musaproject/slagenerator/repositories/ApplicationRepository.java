package eu.musaproject.slagenerator.repositories;

import eu.musaproject.model.Application;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ApplicationRepository extends JpaRepository<Application, Long> {

	Application findByKanbanId(String kanban_id);
	Application findByMacmId(String macmId);
	Application findOne(Long appId);

}
