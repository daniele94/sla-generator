package eu.musaproject.slagenerator.repositories;

import eu.musaproject.model.Component;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ComponentRepository extends JpaRepository<Component, Long> {
	
    List<Component> findByApplicationId(Long app_id);
    Component findByApplicationIdAndKanbanId(Long app_id, String kanban_id);
    Component findByKanbanId(String kanban_id);
    Component findByApplicationIdAndName(Long app_id, String name);
    Component findByApplicationKanbanIdAndKanbanId(String app_kanban_id, String component_kanban_id);
	Component findOne(Long componentId);
    
}
