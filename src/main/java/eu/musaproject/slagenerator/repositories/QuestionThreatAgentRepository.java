package eu.musaproject.slagenerator.repositories;

import eu.musaproject.model.QuestionThreatAgent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
public interface QuestionThreatAgentRepository extends JpaRepository<QuestionThreatAgent, Long> {

	
	@Query(value = "SELECT * FROM question_threat_agents", nativeQuery = true )
	List<QuestionThreatAgent> getquestions();

    QuestionThreatAgent findByquestionId(String questionId);

	QuestionThreatAgent findByquestion(String question);
}
