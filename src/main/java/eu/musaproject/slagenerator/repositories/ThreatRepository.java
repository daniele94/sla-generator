package eu.musaproject.slagenerator.repositories;


import eu.musaproject.model.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Set;

public interface ThreatRepository extends JpaRepository<Threat, Long> {
	
	Threat findByThreatId(String threatId);
	
	@Query(value = "SELECT * FROM threats_strides", nativeQuery = true )
	List<String> getThreatStrides();
	
	List<Threat> findByComponentTypes(Set<ComponentType> componentType);
	
	List<Threat> findByQuestions(Set<QuestionThreat> questionThreat);


	@Query(value ="SELECT DISTINCT *" +
			" from threats inner join threatprotocol on threats.id=threatprotocol.threat inner join protocols " +
			"on threatprotocol.protocol=protocols.id inner join relation on relation.protocol_id=protocols.id" +
			" inner join components on components.id=relation.component_id where components.id=?1 and " +
			"((threatprotocol.role=\"all\") or threatprotocol.role=?3) and threatprotocol.protocol=?2",nativeQuery = true)
	List<Threat> findThreatsProtocolByComponent(Long compId,Protocol protocol,String role);

}
