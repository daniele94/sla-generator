package eu.musaproject.slagenerator.repositories;

import eu.musaproject.model.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface Ta_has_attributeRepository extends JpaRepository<Ta_has_attribute, Long> {

    @Query("select c from Ta_has_attribute c where (c.ta_category=?1)")
    List<Ta_has_attribute> getAttributes(ThreatAgentCategory tac);
}
