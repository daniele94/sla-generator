package eu.musaproject.slagenerator.repositories;

import eu.musaproject.model.ThreatAgentCategory;
import org.springframework.data.jpa.repository.JpaRepository;

import eu.musaproject.model.ThreatAgentAttribute;


public interface ThreatAgentAttributeRepository extends JpaRepository<ThreatAgentAttribute, Long>
{
    ThreatAgentAttribute findById(long l);
    ThreatAgentAttribute findByscoreLabel(String name);


}
