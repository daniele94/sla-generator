package eu.musaproject.slagenerator.repositories;

import eu.musaproject.model.QuestionThreat;
import org.springframework.data.jpa.repository.JpaRepository;

public interface QuestionThreatRepository extends JpaRepository<QuestionThreat, Long> {

	QuestionThreat findByQuestionId(String questionId);
}
