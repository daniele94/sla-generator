package eu.musaproject.slagenerator.repositories;

import eu.musaproject.model.ComponentType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ComponentTypeRepository extends JpaRepository<ComponentType, Long> {

	ComponentType findByName(String name);
	
	@Query(value="SELECT name FROM component_types", nativeQuery = true)
	List<String> getAllComponentTypes();
	
	@Query(value = "SELECT * FROM component_types_threats", nativeQuery = true )
	List<String> getComponentTypeThreads();

}
