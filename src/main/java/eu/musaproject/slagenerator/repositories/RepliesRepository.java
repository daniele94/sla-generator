package eu.musaproject.slagenerator.repositories;

import eu.musaproject.model.QuestionThreatAgent;
import eu.musaproject.model.Replies;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;

public interface RepliesRepository extends JpaRepository<Replies, Long> {

    @Query(value = "SELECT * FROM replies", nativeQuery = true )
    List<Replies> getreplies();

    Replies findByreply(String replies);
}
