package eu.musaproject.slagenerator.repositories;

import eu.musaproject.model.ComponentControl;
import eu.musaproject.model.ComponentControlQuestion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface ComponentControlQuestionRepository extends JpaRepository<ComponentControlQuestion, Long> {
	
	@Modifying
    @Transactional
    @Query("delete from ComponentControlQuestion ctq where ctq.componentControl = ?1")
    void deleteComponentControlQuestionsByComponentControl(ComponentControl componentControl);

	ComponentControlQuestion findOne(Long questionId);
}
