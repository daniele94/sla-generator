package eu.musaproject.slagenerator.repositories;

import eu.musaproject.model.Application;
import eu.musaproject.model.Protocol;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProtocolRepository extends JpaRepository<Protocol, Long> {


    Protocol findByprotocol(String protocol);

}
