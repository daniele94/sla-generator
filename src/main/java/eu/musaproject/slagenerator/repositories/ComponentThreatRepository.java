package eu.musaproject.slagenerator.repositories;

import eu.musaproject.model.Component;
import eu.musaproject.model.ComponentThreat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface ComponentThreatRepository extends JpaRepository<ComponentThreat, Long> {
	
	@Modifying
    @Transactional
    @Query("delete from ComponentThreat ct where ct.component = ?1")
    void deleteComponentThreatsByComponent(Component component);
	
}
