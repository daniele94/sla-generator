package eu.musaproject.slagenerator.repositories;

import eu.musaproject.model.QuestionControl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface QuestionControlRepository extends JpaRepository<QuestionControl, Long> {

	QuestionControl findBySourceId(String sourceId);
	
	@Query(value = "SELECT * FROM questions_control_component_types", nativeQuery = true )
	List<String> getComponentTypes();
}
