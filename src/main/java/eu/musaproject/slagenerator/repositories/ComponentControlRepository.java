package eu.musaproject.slagenerator.repositories;

import eu.musaproject.model.Component;
import eu.musaproject.model.ComponentControl;
import eu.musaproject.model.Control;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Repository
public interface ComponentControlRepository extends JpaRepository<ComponentControl, Long> {
	
	
	List<ComponentControl> findComponentControlByComponent(Component component);
	
	List<ComponentControl> findComponentControlByComponentAndControl(Component component, Control control);
	
	@Modifying
    @Transactional
    @Query("delete from ComponentControl ct where ct.component = ?1")
    void deleteComponentControlsByComponent(Component component);
}
