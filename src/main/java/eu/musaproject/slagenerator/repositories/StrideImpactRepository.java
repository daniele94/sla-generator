package eu.musaproject.slagenerator.repositories;

import eu.musaproject.model.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.List;

public interface StrideImpactRepository extends JpaRepository<StrideImpact, Long> {


    @Modifying
    @Transactional
    @Query("delete from StrideImpact si where si.application= ?1")
    void deleteByApp(Application application);


    @Query("select c from StrideImpact c where (c.application=?1)")
    List<StrideImpact> findByApp(Application application);

}
