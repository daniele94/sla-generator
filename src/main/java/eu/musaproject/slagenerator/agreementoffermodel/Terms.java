package eu.musaproject.slagenerator.agreementoffermodel;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by adispataru on 4/24/15.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "all",
})
@XmlRootElement(name = "Terms")
public class Terms implements Serializable {

    private static final long serialVersionUID = -8689879354763579624L;
    //@XmlElement(name = "All", namespace = "http://schemas.ggf.org/graap/2007/03/ws-agreement")
    @XmlElement(name = "All", namespace = "http://schemas.ggf.org/graap/2007/03/ws-agreement")
    private All all;

    public All getAll() {

        return all;
    }

    public void setAll(All all) {
        this.all = all;
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(propOrder = {"all",})
    @XmlRootElement(name = "All", namespace = "http://schemas.ggf.org/graap/2007/03/ws-agreement")
    public static class All implements Serializable{
        private static final long serialVersionUID = 7854573354027165215L;
        @XmlElementRef(name = "All", namespace = "http://schemas.ggf.org/graap/2007/03/ws-agreement")
        private List<Term> all;

        public List<Term> getAll() {
            if(all == null)
                all = new ArrayList<Term>();
            return all;
        }

        public void setAll(List<Term> all) {
            this.all = all;
        }
    }
}
