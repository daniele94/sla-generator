package eu.musaproject.slagenerator.agreementoffermodel;



import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by adispataru on 4/24/15.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {
        "name", "serviceName", "variableSet"
})
@XmlRootElement(name = "ServiceProperties", namespace = "http://schemas.ggf.org/graap/2007/03/ws-agreement")
public class ServiceProperties extends Term implements Serializable {

    private static final long serialVersionUID = -4678604272898913446L;
    @XmlAttribute(name = "Name", namespace = "http://schemas.ggf.org/graap/2007/03/ws-agreement")
    private String name;
    @XmlAttribute(name = "ServiceName", namespace = "http://schemas.ggf.org/graap/2007/03/ws-agreement")
    private String serviceName;
    @XmlElement(namespace = "http://schemas.ggf.org/graap/2007/03/ws-agreement", name = "VariableSet")
    private VariableSet variableSet;


    public VariableSet getVariableSet() {
        return variableSet;
    }

    public void setVariableSet(VariableSet variableSet) {
        this.variableSet = variableSet;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "VariableSet", propOrder = {
            "variables"
    }, namespace = "http://schemas.ggf.org/graap/2007/03/ws-agreement")
    @XmlRootElement
    public static class VariableSet implements Serializable{
        private static final long serialVersionUID = 5722715325906069384L;
        @XmlElementRef(name = "VariableSet", namespace = "http://schemas.ggf.org/graap/2007/03/ws-agreement")
        private List<Variable> variables;

        public List<Variable> getVariables(){
            if (variables == null)
                variables = new ArrayList<>();
            return variables;
        }

        public void setVariables(List<Variable> variables){
            this.variables = variables;
        }




    }
}
