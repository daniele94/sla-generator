package eu.musaproject.slagenerator.agreementoffermodel;


import javax.xml.bind.annotation.*;
import java.io.Serializable;

/**
 * Created by adispataru on 4/24/15.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "objectiveList",
})
@XmlRootElement(name = "CustomServiceLevel", namespace = "http://schemas.ggf.org/graap/2007/03/ws-agreement")
public class CustomServiceLevel implements Serializable{

        private static final long serialVersionUID = -1103993681197339463L;
        @XmlElement(name = "objectiveList",  namespace = "http://www.specs-project.eu/resources/schemas/xml/SLAtemplate")
        private ObjectiveList objectiveList;

        public ObjectiveList getObjectiveList() {
                return objectiveList;
        }

        public void setObjectiveList(ObjectiveList objectiveList) {
                this.objectiveList = objectiveList;
        }
}
