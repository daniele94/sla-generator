package eu.musaproject.slagenerator.agreementoffermodel;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.math.BigInteger;


/**
 * <p>Java class for NISTSecurityControl complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="NISTSecurityControl">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="control_description" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="name" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="control_family_id" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="control_family_name" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="security_control" use="required" type="{http://www.w3.org/2001/XMLSchema}integer" />
 *       &lt;attribute name="control_enhancement" use="required" type="{http://www.w3.org/2001/XMLSchema}integer" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NISTsecurityControl", namespace = "http://www.specs-project.eu/resources/schemas/xml/control_frameworks/nist", propOrder = { "id",
         "name", "securityControl", "controlEnhancement", "controlFamily", "controlDescription", "importanceWeight"
})
@XmlRootElement(name = "NISTsecurityControl", namespace = "http://www.specs-project.eu/resources/schemas/xml/SLAtemplate")
public class NISTSecurityControl extends AbstractSecurityControl implements Serializable{

    private static final long serialVersionUID = -4912007320478742086L;
    @XmlAttribute(name = "id", required = true)//, namespace = "http://www.specs-project.eu/resources/schemas/xml/control_frameworks/nist")
    protected String id;
    @XmlAttribute(name = "name", required = true)//, namespace = "http://www.specs-project.eu/resources/schemas/xml/control_frameworks/nist")
    protected String name;
    @XmlAttribute(name = "control_family", required = true)//, namespace = "http://www.specs-project.eu/resources/schemas/xml/control_frameworks/nist")
    protected String controlFamily;
//    @XmlAttribute(name = "control_family_name", required = true, namespace = "http://www.specs-project.eu/resources/schemas/xml/control_frameworks/nist")
//    protected String controlFamilyName;
    @XmlAttribute(name = "securityControl", required = true)//, namespace = "http://www.specs-project.eu/resources/schemas/xml/control_frameworks/nist")
    protected String securityControl;
    @XmlAttribute(name = "control_enhancement", required = true)//, namespace = "http://www.specs-project.eu/resources/schemas/xml/control_frameworks/nist")
    protected String controlEnhancement;
    @XmlElement(name = "description", namespace = "http://www.specs-project.eu/resources/schemas/xml/control_frameworks/nist", required = true)
    protected String controlDescription;
    @XmlElement(name = "importance_weight", namespace = "http://www.specs-project.eu/resources/schemas/xml/control_frameworks/nist", required = true)
    @XmlSchemaType(name = "string")
    protected WeightType importanceWeight;


//    public String getControlFamilyName() {
//        return controlFamilyName;
//    }
//
//    public void setControlFamilyName(String controlFamilyName) {
//        this.controlFamilyName = controlFamilyName;
//    }

    public String getControlDescription() {
        return controlDescription;
    }

    public void setControlDescription(String controlDescription) {
        this.controlDescription = controlDescription;
    }

    public WeightType getImportanceWeight() {
        return importanceWeight;
    }

    public void setImportanceWeight(WeightType importanceWeight) {
        this.importanceWeight = importanceWeight;
    }

    /**
     * Gets the value of the controlDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */

    public String getControlFamily() {
        return controlFamily;
    }

    public void setControlFamily(String controlFamily) {
        this.controlFamily = controlFamily;
    }


    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }



    /**
     * Gets the value of the CCMSecurityControl property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public String getSecurityControl() {
        return securityControl;
    }

    /**
     * Sets the value of the CCMSecurityControl property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSecurityControl(String value) {
        this.securityControl = value;
    }

    /**
     * Gets the value of the controlEnhancement property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public String getControlEnhancement() {
        return controlEnhancement;
    }

    /**
     * Sets the value of the controlEnhancement property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setControlEnhancement(String value) {
        this.controlEnhancement = value;
    }

}
