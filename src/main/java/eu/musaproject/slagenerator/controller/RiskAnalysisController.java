package eu.musaproject.slagenerator.controller;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import eu.musaproject.enumeration.ComponentState;
import eu.musaproject.model.*;
import eu.musaproject.slagenerator.entity.JsonResponse;
import eu.musaproject.slagenerator.entity.Status;
import eu.musaproject.slagenerator.repositories.*;
import eu.musaproject.slagenerator.service.RestClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import java.lang.reflect.Type;
import java.text.DecimalFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@RequestMapping("/sla-generator/api/riskAnalysis")
public class RiskAnalysisController {

	public static final Logger logger = LoggerFactory.getLogger(RiskAnalysisController.class);

	@Autowired ThreatRepository threatRepository;
	@Autowired ComponentRepository componentRepository;
	@Autowired ComponentControlMetricRepository componentControlMetricRepository;
	@Autowired ComponentControlQuestionRepository componentControlQuestionRepository;
	@Autowired RestClient restClient;
	@Autowired ComponentThreatRepository componentThreatRepository;
	@Autowired ComponentControlRepository componentControlRepository;
	@Autowired ControlRepository controlRepository;
	@Autowired ApplicationRepository applicationRepository;
	@Autowired ta_category_applies_on_application_Repository ta_category_applies_on_application_Repository;
	@Autowired ThreatAgentCategoryRepository ThreatAgentCategoryRepository;
	@Autowired Ta_has_attributeRepository Ta_has_attributeRepository;
	@Autowired ThreatAgentAttributeRepository ThreatAgentAttributeRepository;
	@Autowired eu.musaproject.slagenerator.repositories.StrideImpactRepository StrideImpactRepository;
	@Autowired eu.musaproject.slagenerator.repositories.RelationRepository RelationRepository;
	@Autowired eu.musaproject.slagenerator.repositories.ProtocolRepository ProtocolRepository;

	@RequestMapping(path="/componentQuestions", method = RequestMethod.GET)
	public ResponseEntity<?> getComponentQuestions(@RequestParam(value = "componentId") Long componentId, UriComponentsBuilder ucBuilder) {

		System.out.println("Get Questions List called: "+componentId);

		Component component = componentRepository.findOne(componentId);

		//		if(component.getState() != ComponentState.CREATED){
		//Store the metrics for the component
		ComponentType compType = component.getComponentType();
		Set<ComponentType> types = new HashSet<ComponentType>();
		types.add(compType);
		List<Threat> threats = threatRepository.findByComponentTypes(types);
		List<QuestionThreat> questions = new ArrayList<QuestionThreat>();
		for(Threat threat : threats){
			for(QuestionThreat questionThreat : threat.getQuestions()){
				if(!questions.contains(questionThreat)){
					questions.add(questionThreat);
				}
			}
		}

		return new ResponseEntity<List<QuestionThreat>>(questions, HttpStatus.OK);

		//		}else{
		//			return new ResponseEntity<JsonResponse>(new JsonResponse(Status.KO, "The component is in CREATED state. No risk profile associated to the component found!", null), HttpStatus.BAD_REQUEST);
		//		}
	}


	@RequestMapping(path="/calculateScores", method = RequestMethod.POST)
	public ResponseEntity<?> calculateThreatAgentScore (@RequestParam(value = "appId") Long appId) {

		System.out.println("Get Scores List ");

		List<ta_category_applies_on_application> listOfCategoriesonApp=ta_category_applies_on_application_Repository.getCategories(appId);

		List<ThreatAgentCategory> listOfCategories=new ArrayList<>();
		HashMap<ThreatAgentCategory, List<ThreatAgentAttribute>> Category_Attributes = new HashMap<>();

		for(ta_category_applies_on_application tac:listOfCategoriesonApp)
		{
			listOfCategories.add(ThreatAgentCategoryRepository.findByname(tac.getTa().getName()));
		}
		List<List<Ta_has_attribute>> listofLists=new ArrayList<>();
		//create a list of lists of associations of ThreatAgent and Attributes
		for(ThreatAgentCategory tac:listOfCategories)
		{
			listofLists.add(Ta_has_attributeRepository.getAttributes(tac));
		}
		for(List<Ta_has_attribute> lista:listofLists)
		{
			List <ThreatAgentAttribute> listofAttributes=new ArrayList<>();
			String CategoryName = null;

			for(Ta_has_attribute element:lista)
			{
				listofAttributes.add(ThreatAgentAttributeRepository.findById(element.getTaAttribute().getId()));
				//System.out.println(element.getTa_category().getName()+" "+ element.getTaAttribute().getScoreLabel());
				CategoryName=element.getTa_category().getName();
			}
			Category_Attributes.put(ThreatAgentCategoryRepository.findByname(CategoryName),listofAttributes);
		}
		//		HashMap<ThreatAgentCategory, List<ThreatAgentAttribute>> Category_Attributes = new HashMap<>();
		double motive,size,opportunity,skill_level;
		List<Score> scores=new ArrayList<>();


		for (Map.Entry<ThreatAgentCategory, List<ThreatAgentAttribute>> entry : Category_Attributes.entrySet())
		{
			ThreatAgentCategory key = entry.getKey();
			List<ThreatAgentAttribute> value = entry.getValue();
			List<Double> scores_outcome=new ArrayList<>();
			List<Double> scores_objective=new ArrayList<>();
			double intent=0;
			double objectives=0;
			double skills=0;
			double access=0;
			double resources=0;
			double outcome=0;
			double visibility=0;
			double limits=0;
			for(ThreatAgentAttribute v:value)
			{
					switch (v.getAttribute())
					{
						case "Intent":
							intent=v.getScore();
							break;
						case "Objectives":
							objectives=v.getScore();
							scores_objective.add(objectives);
							break;
						case "Skills":
							skills=v.getScore();
							break;
						case "Access":
							access=v.getScore();
							break;
						case "Resources":
							resources=v.getScore();
							break;
						case "Outcome":
							outcome=v.getScore();
							scores_outcome.add(outcome);
							break;
						case "Visibility":
							visibility=v.getScore();
							break;
						case "Limits":
							limits=v.getScore();
							break;
					}
				//System.out.println("Chiave "+key.getName() +" Value "+v.getScoreLabel()+" CON VALORE "+v.getScore()+" ATTRIBUTO "+v.getAttribute());
			}

			outcome=0;
			objectives=0;
			for(double score: scores_outcome)
			{
				outcome+=score;
			}

			outcome/=scores_outcome.size();

			for(double score: scores_objective)
			{
				objectives+=score;
			}
			objectives/=scores_objective.size();

			skill_level=(skills/4) *10;
			size=(resources/6)* 10;

			opportunity= (((access/2) + (resources/6) + (visibility/4)) /3)*10;
			motive= (((limits/4)+(intent/2)+(outcome/5))/3)*10;

			//System.out.println("La categoria "+key.getName()+" ha valore OWASP SKILLS pari a "+skill_level+" con size "+size+ " opportunity "+opportunity + " motivo "+ motive);
			ta_category_applies_on_application_Repository.update(motive,opportunity,size,skill_level,key.getId());
			int weight=0;
			String weight_string=(ta_category_applies_on_application_Repository.findByta(key).getCategory_weight());
			if(weight_string.equals("Low"))
			{
				weight=1;
			}
			if(weight_string.equals("Medium"))
			{
				weight=2;
			}
			if(weight_string.equals("High"))
			{
				weight=3;
			}
			scores.add(new Score(motive,size,opportunity,skill_level,weight));
		}


		double skill_level_tot=0;
		double size_tot=0;
		double opportunity_tot=0;
		double motive_tot=0;
		int total_weight=0;
		for(Score score:scores)
		{
			total_weight+=score.weight;

			skill_level_tot+=score.weight*score.skill_level;

			size_tot= size_tot+ score.weight*score.size;

			opportunity_tot+=score.weight*score.opportunity;
			motive_tot+=score.weight*score.motive;
		}
		skill_level_tot/= (total_weight);
		size_tot/=(total_weight);
		opportunity_tot/= (total_weight);
		motive_tot/=(total_weight);



		return new ResponseEntity<Score> (new Score((motive_tot),(size_tot),(opportunity_tot),(skill_level_tot),total_weight), HttpStatus.OK);
	}

	@RequestMapping(path="/saveImpactEvaluation", method = RequestMethod.POST)
	public ResponseEntity<?> saveImpactEvaluation(@RequestParam(value = "appId") Long appId,
											 	  @RequestParam(value = "impactScores") String impactScores) throws JSONException {

		//impactScores="["+impactScores+"]";
		logger.info("save impact called: "+impactScores+" "+appId);
		Application app=applicationRepository.findOne(appId);
		if(app!=null)
		{

		StrideImpactRepository.deleteByApp(app);


		ArrayList<String> impact_keys= new ArrayList<>(Arrays.asList("financialdamage","reputationdamage"
				,"noncompliance","privacyviolation"));

		ArrayList<String> stride_keys= new ArrayList<>(Arrays.asList("spoofing","tampering","Reputation",
				"informationDisclosure","dos","elevationOfPrivileges"));
		for(String skey:stride_keys)
		{
			String stride_string=null;
			StrideImpact strideimpactObj=new StrideImpact();
			switch(skey)
			{
				case "spoofing":
					stride_string="Spoofing";
					break;
				case "tampering":
					stride_string="Tampering";
					break;
				case "Reputation":
					stride_string="Reputation";
					break;
				case "informationDisclosure":
					stride_string="Information Disclosure";
					break;
				case "dos":
					stride_string="Denial Of Service";
					break;
				case "elevationOfPrivileges":
					stride_string="Elevation Of Privileges";
					break;
				default: stride_string=null;

			}
			strideimpactObj.setStride_category(stride_string);


			for(String ikey:impact_keys)
			{

					JSONObject obj = new JSONObject(impactScores);
					String skey_string= obj.getJSONObject(skey).toString();
					JSONObject obj2 = new JSONObject(skey_string);
					//System.out.println("Categoria "+skey+" Impatto su "+ikey+" Punteggio "+obj2.getInt(ikey));

				switch(ikey)
				{
					case "financialdamage":
						strideimpactObj.setFinancialdamage_score(obj2.getInt("financialdamage"));
						break;
					case "reputationdamage":
						strideimpactObj.setReputationdamage_score(obj2.getInt("reputationdamage"));
						break;
					case "noncompliance":
						strideimpactObj.setNoncompliance_score(obj2.getInt("noncompliance"));
						break;
					case "privacyviolation":
						strideimpactObj.setPrivacyviolation_score(obj2.getInt("privacyviolation"));
						break;
					default:

				}

			}
			strideimpactObj.setApplication(app);
			StrideImpactRepository.save(strideimpactObj);

		}
		}

		return new ResponseEntity<Object>(null, HttpStatus.OK);
	}


	@RequestMapping(path="/submitQuestions", method = RequestMethod.POST)
	public ResponseEntity<?> submitQuestions(@RequestParam(value="appId") Long appId,
											@RequestParam(value = "compId") Long compId,
											 @RequestParam(value = "threatQuestions") String threatQuestions, UriComponentsBuilder ucBuilder) {

		logger.info("submitQuestions called: "+threatQuestions);
		Type listType = new TypeToken<ArrayList<QuestionThreat>>(){}.getType();
		Application application=applicationRepository.findOne(appId);

		List<ComponentThreat> componentThreats = new ArrayList<ComponentThreat>();

		if(application!=null)
		{

		List<QuestionThreat> questionThreats = new Gson().fromJson(threatQuestions, listType);

		//create array of threats that verify questions
		List<Threat> threats = new ArrayList<Threat>();
		for(QuestionThreat questionThreat : questionThreats){
			if(questionThreat.getSelected()){
				Set<QuestionThreat> questions = new HashSet<QuestionThreat>();
				questions.add(questionThreat);
				List<Threat> threatsQ = threatRepository.findByQuestions(questions);
				for(Threat threat : threatsQ){
					if(!threats.contains(threat)){
						threats.add(threat);
					}
				}
			}
		}

		Component component = componentRepository.findOne(compId);
		componentThreatRepository.deleteComponentThreatsByComponent(component);


			List<Relation> relations = RelationRepository.findByComponent(compId);

			for(Relation rel:relations)
			{
			//	System.out.println(rel.getProtocol().getProtocol()+" "+rel.getRole().getrole()+" "+rel.getComponent().getName());
			}

			/*for each relation of component create a list of threat for relation protocol
			and merge (avoiding duplicates) with threats list of the component
			 */

			for(Relation relation:relations)
			{
					try
					{
					//System.out.println(relation.getProtocol().getProtocol());
					if(relation!=null)
					{

						System.out.println("protocollo "+ relation.getProtocol().getProtocol());

						Protocol protocol=ProtocolRepository.findByprotocol(relation.getProtocol().getProtocol());
						List<Threat> threats_protocol=threatRepository.findThreatsProtocolByComponent(compId,protocol,relation.getRole().getrole());

						for(Threat tp:threats_protocol)
						{
							System.out.println(tp.getThreatId()+" "+tp.getThreatName()+" "+relation.getProtocol().getProtocol()+" "+
									relation.getRole().getrole());
						}

						threats=this.union(threats,threats_protocol);


						System.out.println(relation.getComponent().getName()+" "+relation.getProtocol().getProtocol()+" "+relation.getRole().getrole());
						}
					} catch (NullPointerException e)
					{
						e.printStackTrace();
					}
				}


		List<StrideImpact> strideImpacts=StrideImpactRepository.findByApp(application);

		/*
		for(StrideImpact si:strideImpacts)
		{
			System.out.println("Categoria "+si.getStride_category()+" "+si.getFinancialdamage_score()+" "+si.getNoncompliance_score() +" "+
					si.getPrivacyviolation_score()+" "+si.getReputationdamage_score());
		}

		 */

		for(Threat threat : threats){
			Set<Stride> strides=threat.getStrides();
			//search max starting by first category of threat found in strideImpacts
			int firstIndexMax=getFirstStrideIndex(strideImpacts,strides);
			int FinancialDamageMax= ((strideImpacts.get(firstIndexMax).getFinancialdamage_score()));
			int  NonComplianceMax= ((strideImpacts.get(firstIndexMax).getNoncompliance_score()));
			int  PrivacyMax= ((strideImpacts.get(firstIndexMax).getPrivacyviolation_score()));
			int  ReputationMax= ((strideImpacts.get(firstIndexMax).getReputationdamage_score()));
			String threat_stride=null;
			for(Stride stride:strides)
			{
				threat_stride=stride.getName();

				for(StrideImpact si:strideImpacts)
				{
						if(threat_stride.equalsIgnoreCase(si.getStride_category()))
						{

								if((si.getFinancialdamage_score())>FinancialDamageMax)
								{
									FinancialDamageMax=(si.getFinancialdamage_score());
								}
								if((si.getNoncompliance_score())>NonComplianceMax)
								{
									NonComplianceMax=(si.getNoncompliance_score());

								}
								if((si.getPrivacyviolation_score())>PrivacyMax)
								{
									PrivacyMax=(si.getPrivacyviolation_score());

								}
								if(si.getReputationdamage_score()>ReputationMax)
								{
									ReputationMax=si.getReputationdamage_score();

								}
						}

				}


			}
			ComponentThreat componentThreat = new ComponentThreat(threat);

			/*System.out.println("Sono il threat "+threat.getThreatId()+" classificato come "+threat_stride+
					" ed i miei valori sono "+FinancialDamageMax+" "+
					NonComplianceMax+" "+PrivacyMax+" "+ReputationMax);*/

			componentThreat.setFinancialDamage(FinancialDamageMax);
			componentThreat.setNonCompliance(NonComplianceMax);
			componentThreat.setReputationDamage(ReputationMax);
			componentThreat.setPrivacyViolation(PrivacyMax);

			componentThreats.add(componentThreat);
			component.addComponentThreat(componentThreat);
		}
		componentRepository.save(component);
		}

		return new ResponseEntity<List<ComponentThreat>>(componentThreats, HttpStatus.OK);
	}

	@RequestMapping(path="/saveEvaluation", method = RequestMethod.POST)
	public ResponseEntity<?> saveEvaluation(@RequestParam(value = "compId") Long compId, 
			@RequestParam(value = "componentThreats") String componentThreatsString, 
			@RequestParam(value = "spoofingRisk") String spoofingRisk,
			@RequestParam(value = "tamperingRisk") String tamperingRisk,
			@RequestParam(value = "repudiationRisk") String repudiationRisk,
			@RequestParam(value = "informationDisclosureRisk") String informationDisclosureRisk,
			@RequestParam(value = "denialOfServiceRisk") String denialOfServiceRisk,
			@RequestParam(value = "elevationOfPrivilegesRisk") String elevationOfPrivilegesRisk,
			UriComponentsBuilder ucBuilder) {

		logger.info("saveEvaluation called: "+componentThreatsString);
		Type listType = new TypeToken<ArrayList<ComponentThreat>>(){}.getType();

		List<ComponentThreat> componentThreats = new Gson().fromJson(componentThreatsString, listType);

		List<Control> controls = new ArrayList<Control>();
		List<String> controlsId = new ArrayList<String>();

		Component component = componentRepository.findOne(compId);
		componentThreatRepository.deleteComponentThreatsByComponent(component);
		for(ComponentThreat componentThreat : componentThreats){
			component.addComponentThreat(componentThreat);
			for(Control control : componentThreat.getThreat().getSuggestedControls()){
				String controlRisk = control.getRisk();
				
				String riskToConsider = "LOW";
				for(Stride stride : componentThreat.getThreat().getStrides()){
					if(stride.getName().equals("SPOOFING")){
						if(spoofingRisk.equals("MEDIUM")){
							riskToConsider = "MOD";
						}else if(spoofingRisk.equals("HIGH") || spoofingRisk.equals("CRITICAL")){
							riskToConsider = "HIGH";
						}
					}else if(stride.getName().equals("INFORMATION DISCLOSURE")){
						if(informationDisclosureRisk.equals("MEDIUM")){
							riskToConsider = "MOD";
						}else if(informationDisclosureRisk.equals("HIGH") || informationDisclosureRisk.equals("CRITICAL")){
							riskToConsider = "HIGH";
						}
					}else if(stride.getName().equals("ELEVATION OF PRIVILEGES")){
						if(elevationOfPrivilegesRisk.equals("MEDIUM")){
							riskToConsider = "MOD";
						}else if(elevationOfPrivilegesRisk.equals("HIGH") || elevationOfPrivilegesRisk.equals("CRITICAL")){
							riskToConsider = "HIGH";
						}
					}else if(stride.getName().equals("DENIAL OF SERVICE")){
						if(denialOfServiceRisk.equals("MEDIUM")){
							riskToConsider = "MOD";
						}else if(denialOfServiceRisk.equals("HIGH") || denialOfServiceRisk.equals("CRITICAL")){
							riskToConsider = "HIGH";
						}
					}else if(stride.getName().equals("TAMPERING")){
						if(tamperingRisk.equals("MEDIUM")){
							riskToConsider = "MOD";
						}else if(tamperingRisk.equals("HIGH") || tamperingRisk.equals("CRITICAL")){
							riskToConsider = "HIGH";
						}
					}else if(stride.getName().equals("REPUDIATION")){
						if(repudiationRisk.equals("MEDIUM")){
							riskToConsider = "MOD";
						}else if(repudiationRisk.equals("HIGH") || repudiationRisk.equals("CRITICAL")){
							riskToConsider = "HIGH";
						}
					}
				}

				if(controlRisk.equals("LOW")){
					if(!controlsId.contains(control.getControlId())){
						control.setSelected(true);
						controls.add(control);
						controlsId.add(control.getControlId());
					}
				}else if(controlRisk.equals("MOD") && (riskToConsider.equals("MOD") || riskToConsider.equals("HIGH"))){
					if(!controlsId.contains(control.getControlId())){
						control.setSelected(true);
						controls.add(control);
						controlsId.add(control.getControlId());
					}
				}else if(controlRisk.equals("HIGH") && riskToConsider.equals("HIGH")){
					if(!controlsId.contains(control.getControlId())){
						control.setSelected(true);
						controls.add(control);
						controlsId.add(control.getControlId());
					}
				}
			}
		}
		componentRepository.save(component);


		return new ResponseEntity<List<Control>>(controls, HttpStatus.OK);
	}

	@RequestMapping(path="/submitControls", method = RequestMethod.POST)
	public ResponseEntity<JsonResponse> submitControls(@RequestParam(value = "compId") Long compId,
													   @RequestParam(value = "controls") String controlsString,
													   @RequestParam(value = "jwtToken") String jwtToken,
													   @RequestParam(value = "userId") String userId, UriComponentsBuilder ucBuilder) throws JSONException {

		logger.info("submitControls called: "+controlsString);
		Type listType = new TypeToken<ArrayList<Control>>(){}.getType();

		List<Control> controls = new Gson().fromJson(controlsString, listType);


		Component component = componentRepository.findOne(compId);
		for(ComponentControl componentCntrol : component.getComponentControls()){
			componentControlMetricRepository.deleteComponentControlMetricsByComponentControl(componentCntrol);
		}
		for(ComponentControl componentCntrol : component.getComponentControls()){
			componentControlQuestionRepository.deleteComponentControlQuestionsByComponentControl(componentCntrol);
		}
		componentControlRepository.deleteComponentControlsByComponent(component);

		List<ComponentControl> componentControls = new ArrayList<ComponentControl>();
		for(Control control : controls){
			if(control.getSelected()){
				Control repoControl = controlRepository.findOne(control.getId());
				ComponentControl componentControl = new ComponentControl(repoControl);
				componentControls.add(componentControl);
				component.addComponentControl(componentControl);
			}
		}
		if(component.getApplication().getKanbanId() != null && !component.getApplication().getKanbanId().equals("")){
			String errorMessage = restClient.updateSession(component.getApplication().getKanbanId(), component.getKanbanId(), jwtToken, userId, "PROFILED", null, null, null);
			if(errorMessage != null){
					return new ResponseEntity<JsonResponse>(new JsonResponse(Status.KO, errorMessage, null), HttpStatus.BAD_REQUEST);
			}
		}
		component.setState(ComponentState.PROFILED);
		componentRepository.save(component);

		return ResponseEntity.status(HttpStatus.OK).body(new JsonResponse(Status.OK, "Controls saved successfully", null));
	}

	private class Score {
		private double motive;
		private double size;
		private double opportunity;
		private double skill_level;
		private int weight;

		public Score(double motive,double size,double opportunity,double skill_level,int weight)
		{
			this.motive=motive;
			this.size=size;
			this.opportunity=opportunity;
			this.skill_level=skill_level;
			this.weight=weight;
		}

		public double getMotive() {
			return motive;
		}

		public void setMotive(double motive) {
			this.motive = motive;
		}

		public double getSize() {
			return size;
		}

		public void setSize(double size) {
			this.size = size;
		}

		public double getOpportunity() {
			return opportunity;
		}

		public void setOpportunity(double opportunity) {
			this.opportunity = opportunity;
		}

		public double getSkill_level() {
			return skill_level;
		}

		public void setSkill_level(double skill_level) {
			this.skill_level = skill_level;
		}




	}

	static int getFirstStrideIndex(List<StrideImpact> strideImpacts,Set<Stride> strides)
	{
		int index=0;
		for(Stride stride:strides)
		{
			for(StrideImpact si:strideImpacts)
			{
				if(stride.getName().equalsIgnoreCase(si.getStride_category()))
				{
					index=strideImpacts.indexOf(si);
					break;
				}
			}
		}
		return index;
	}

	public static List<Threat> union(List<Threat> list1, List<Threat> list2) {


		for(Threat t2:list2)
		{
			if (!list1.contains(t2))
			{
				list1.add(t2);
			}
		}

		return list1;
	}



}



