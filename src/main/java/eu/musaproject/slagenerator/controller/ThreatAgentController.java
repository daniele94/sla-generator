package eu.musaproject.slagenerator.controller;

import eu.musaproject.model.*;
import eu.musaproject.slagenerator.entity.JsonResponse;
import eu.musaproject.slagenerator.entity.Status;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/sla-generator/api/threatagentwizard")
public class ThreatAgentController {

	public static final Logger logger = LoggerFactory.getLogger(ThreatAgentController.class);

	 @Autowired eu.musaproject.slagenerator.repositories.QuestionThreatAgentRepository QuestionThreatAgentRepository;
	 @Autowired eu.musaproject.slagenerator.repositories.RepliesRepository repliesRepository;
	 @Autowired eu.musaproject.slagenerator.repositories.ApplicationRepository applicationRepository;
	 @Autowired eu.musaproject.slagenerator.repositories.QuestionThreatAgentOnApplicationRepository QuestionThreatAgentOnApplicationRepository;
	 @Autowired eu.musaproject.slagenerator.repositories.ThreatAgentCategoryRepository ThreatAgentCategoryRepository;
	 @Autowired eu.musaproject.slagenerator.repositories.ThreatAgentHasAttributeRepository ThreatAgentHasAttributeRepository;
	 @Autowired eu.musaproject.slagenerator.repositories.ThreatAgentAttributeRepository ThreatAgentAttributeRepository;
	 @Autowired eu.musaproject.slagenerator.repositories.ReplyCategoryRepository ReplyCategoryRepository;
	 @Autowired eu.musaproject.slagenerator.repositories.Ta_has_attributeRepository Ta_has_attributeRepository;
	 @Autowired eu.musaproject.slagenerator.repositories.ta_category_applies_on_application_Repository ta_category_applies_on_application_Repository;


	@RequestMapping(path="/questions", method = RequestMethod.GET)
	public ResponseEntity<?> getQuestions(Long Id, UriComponentsBuilder ucBuilder) {
		System.out.println("Get Questions List");
		List<QuestionThreatAgent> questions=QuestionThreatAgentRepository.findAll();
		return new ResponseEntity<Object>(questions, HttpStatus.OK);
	}


	@RequestMapping(path="/replies", method = RequestMethod.GET)
	public ResponseEntity<?> getReplies(Long Id, UriComponentsBuilder ucBuilder) {

		System.out.println("Get Replies List called:");

		List<Replies> replies=repliesRepository.findAll();

		return new ResponseEntity<Object>(replies, HttpStatus.OK);
	}

	@RequestMapping(path="/submitScoring", method = RequestMethod.POST)
	public ResponseEntity<?> submitScoring(Long Id, UriComponentsBuilder ucBuilder,
										   @RequestParam(value="appId") Long appId,
									@RequestParam(value="CategoryScoring") String CategoryScoring) throws JSONException {

		System.out.println("submitScoring called:");
		CategoryScoring="["+CategoryScoring+"]";

		//JSONArray jsonArr = new JSONArray(CategoryScoring);
		ArrayList<Scores> array=new ArrayList<>();
		JSONArray jsonArr = new JSONArray(CategoryScoring);

		for (int i = 0; i < jsonArr.getJSONObject(0).getJSONArray("scores").length(); i++)
		{
			//create arraylist from JSON OBJECT
			JSONObject JSONObj=jsonArr.getJSONObject(0).getJSONArray("scores").getJSONObject(i);

			array.add(new Scores(JSONObj.get("ThreatAgentName").toString(),JSONObj.get("score").toString()));
			ThreatAgentCategory tac=ThreatAgentCategoryRepository.findByname(JSONObj.get("ThreatAgentName").toString());
			ta_category_applies_on_application_Repository.updateScore((JSONObj.get("score").toString()),tac);

		}





		//update query ta_category_applies_on_application

		return new ResponseEntity<Object>(CategoryScoring, HttpStatus.OK);
	}



	@RequestMapping(path="/attributes", method = RequestMethod.GET)
	public ResponseEntity<?> getAttributes(Long Id, UriComponentsBuilder ucBuilder) {

		System.out.println("Get Attributes List called:");

		List<Ta_has_attribute> attributes= Ta_has_attributeRepository.findAll();
		String LastCategory=attributes.get(attributes.size()-1).getTa_category().getName();
		String LastAttribute=attributes.get(attributes.size()-1).getTaAttribute().getScoreLabel();


		HashMap<String, List<ThreatAgentAttribute>> Ta_has_attributes = new HashMap<>();

		String temp=attributes.get(0).getTa_category().getName();
		List<ThreatAgentAttribute> attributesValue=new ArrayList<>();
		//need observer pattern on categoryName(temp)

		for(Ta_has_attribute attribute:attributes)
		{

			if(temp.equals(attribute.getTa_category().getName()))
			{
				attributesValue.add(attribute.getTaAttribute());
				//System.out.println(attribute.getTa_category().getName()+" "+attribute.getTaAttribute().getScoreLabel());
			}
			else
				{
					attributesValue.add(attribute.getTaAttribute());
					Ta_has_attributes.put(temp,attributesValue);
					attributesValue=new ArrayList<>();
			}

			if(attribute.getTa_category().getName().equals(LastCategory) && attribute.getTaAttribute().getScoreLabel().equals(LastAttribute))
			{
				Ta_has_attributes.put(attribute.getTa_category().getName(),attributesValue);
			}
			temp=attribute.getTa_category().getName();
		}
		return new ResponseEntity<Object>(Ta_has_attributes, HttpStatus.OK);
	}


	@RequestMapping(path="/submitQuestions", method = RequestMethod.POST)
	public ResponseEntity<?> submitQuestions(Long Id, UriComponentsBuilder ucBuilder, @RequestParam(value="appId") Long appId,
	@RequestParam(value = "threatagentReply") String threatagentReply) throws JSONException {
		System.out.println("Controller Submit Question:");
		Question_Threat_Agent_On_Application save = null;


		//make JSON readable
		threatagentReply="["+threatagentReply+"]";
		JSONArray jsonArr = new JSONArray(threatagentReply);
		ArrayList<RepliesChecked> arrayRepliesChecked=new ArrayList<>();
		ArrayList<List<ReplyCategory>> ListOfCategories = new ArrayList<>();

		List <ReplyCategory> listatemp=null;



		for (int i = 0; i < jsonArr.getJSONObject(0).getJSONArray("replies").length(); i++)
		{
			//create arraylist from JSON OBJECT
			JSONObject JSONObj=jsonArr.getJSONObject(0).getJSONArray("replies").getJSONObject(i);
			arrayRepliesChecked.add(new RepliesChecked(JSONObj.get("QuestionId").toString(),JSONObj.get("reply").toString()));
			//System.out.println("Elemento #"+i+" "+jsonObj.getJSONArray("replies").get(i));
		}

		try {
			Application app = applicationRepository.findOne(appId);

			if(app!=null) {
				QuestionThreatAgentOnApplicationRepository.deleteByAppId(appId);
				for (int i = 0; i < arrayRepliesChecked.size(); i++) {
					//save replies from created array
					Question_Threat_Agent_On_Application qta_on_app = new Question_Threat_Agent_On_Application();
					qta_on_app.setApplication(app);
					QuestionThreatAgent qta = QuestionThreatAgentRepository.findByquestionId(arrayRepliesChecked.get(i).question);
					Replies reply = repliesRepository.findByreply(arrayRepliesChecked.get(i).replies);
					qta_on_app.setQta(qta);
					qta_on_app.setReply(reply);
					//System.out.println("Question " + arrayRepliesChecked.get(i).question + " Reply " + arrayRepliesChecked.get(i).replies);
					save=QuestionThreatAgentOnApplicationRepository.save(qta_on_app);


					List<ReplyCategory> TACList=ReplyCategoryRepository.getCategories(arrayRepliesChecked.get(i).replies);


					//System.out.println("Aggiungo "+TACList);
					ListOfCategories.add(TACList);


				}
			}
			else
			{
				System.err.println("Error App NULL");
			}


		List <ReplyCategory> listaunionQ3=ListOfCategories.get(2);
		List <ReplyCategory> listaunionQ4=null;


		int indexQ4=0;
		//union of multiple results of 3 and 4 question.
		for(List<ReplyCategory> lista:ListOfCategories)
		{

			if(lista.get(0).getReply().getQuestion().getQuestionId().equals("Q3"))
			{
				listaunionQ3 = this.union(listaunionQ3, lista);
				//printList(listaunionQ3);
				ListOfCategories.set(2,listaunionQ3);
				//printList(ListOfCategories.get(2));

			}

			if(lista.get(0).getReply().getQuestion().getQuestionId().equals("Q4"))
			{
				if(indexQ4==0)
				{
					listaunionQ4=lista;

				}
				listaunionQ4 = this.union(listaunionQ4, lista);
				//printList(listaunionQ4);
				ListOfCategories.set(3,listaunionQ4);
				//ListOfCategories.
				//(ListOfCategories.get(3));
				indexQ4++;

			}

		}

		listatemp=ListOfCategories.get(0);

		int index=0;

		//intersection first 4 list  related(of questions)

		while(index<4)
		{
				listatemp = this.intersection(listatemp, ListOfCategories.get(index));
				//printList(ListOfCategories.get(index));
				index++;

		}
			QuestionThreatAgentOnApplicationRepository.deleteByAppId(appId);

			ta_category_applies_on_application_Repository.deleteByAppId(appId);

		for(ReplyCategory element: listatemp)
		{
			ta_category_applies_on_application Cat_App = new ta_category_applies_on_application();

			Cat_App.setApplication(app);
			ThreatAgentCategory tac=ThreatAgentCategoryRepository.findByname(element.getThreatAgentCategory().getName());
			Cat_App.setTa(tac);
			ta_category_applies_on_application_Repository.save(Cat_App);

		}

		} catch (MethodArgumentTypeMismatchException e)
		{
			e.printStackTrace();
		}




		if(save != null){
			return ResponseEntity.status(HttpStatus.OK).body(new JsonResponse(Status.OK, "Question saved successfully", listatemp));
		}else{
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new JsonResponse(Status.KO, "Error saving ", null));
		}

	}

	public static void printList(List<ReplyCategory> list)
	{
		for (ReplyCategory t : list) {
			System.out.println(t.getReply().getQuestion().getQuestionId()+" "+t.getThreatAgentCategory().getName());
		}
		System.out.println("\n");
	}

	public static List<ReplyCategory> intersection(List<ReplyCategory> list1, List<ReplyCategory> list2) {
		List<ReplyCategory> list = new ArrayList<ReplyCategory>();
		List<String> listCategory = new ArrayList<>();

		for (ReplyCategory t : list2) {
			listCategory.add(t.getThreatAgentCategory().getName());
		}

		for (ReplyCategory t : list1) {
			if (listCategory.contains(t.getThreatAgentCategory().getName())) {
				list.add(t);
			}
		}

		return list;
	}


	public static List<ReplyCategory> union(List<ReplyCategory> list1, List<ReplyCategory> list2) {
		List<String> listCategory = new ArrayList<>();
		for (ReplyCategory t : list1) {
			listCategory.add(t.getThreatAgentCategory().getName());
		}

			for(ReplyCategory t2:list2)
			{
				if (!listCategory.contains(t2.getThreatAgentCategory().getName()))
				{
					list1.add(t2);
				}
			}

		return list1;
	}


	public class RepliesChecked
	{
		String question;
		String replies;

		public RepliesChecked(String question,
				String replies)
		{
			this.question=question;
			this.replies=replies;
		}
	}


	public class Scores
	{
		String ThreatAgent;
		String Score;

		public Scores(String ThreatAgent,
							  String Score)
		{
			this.ThreatAgent=ThreatAgent;
			this.Score=Score;
		}
	}



}



