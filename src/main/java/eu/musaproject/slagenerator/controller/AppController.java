package eu.musaproject.slagenerator.controller;

import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class AppController {

	@RequestMapping("/")
	String home(ModelMap modal) {
		modal.addAttribute("title","Sla Generator V2");
		return "index";
	}

	@RequestMapping("/{page}")
	String pageHandler(@PathVariable("page") final String page, @RequestHeader HttpHeaders headers) {

		for (String key : headers.keySet()) {
			System.out.println("Key:"+key+" Value:"+headers.get(key));
		}
		
		System.out.println("REFERER of request: "+headers.get(headers.REFERER));
		return page;
	}

}
