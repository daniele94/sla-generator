package eu.musaproject.slagenerator.entity;

import java.util.List;

public class RiskApp {
	
	private String name;
	private List<RiskComponent> items;
	
	public RiskApp(){}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<RiskComponent> getItems() {
		return items;
	}

	public void setItems(List<RiskComponent> items) {
		this.items = items;
	}
	
	
}
