package eu.musaproject.slagenerator.entity;

import java.util.List;

public class RiskComponent {

	private String text;
	private String cid;
	private List<RiskThreat> risks;
	
	public RiskComponent(){}
	
	public class RiskThreat {
		private int id;
		private String threatId;
		private String name;
		private String description;
		private String strideId;
		private String strideName;
		private List<RiskControl> sR;
		
		public RiskThreat(){}

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getThreatId() {
			return threatId;
		}

		public void setThreatId(String threatId) {
			this.threatId = threatId;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		public String getStrideId() {
			return strideId;
		}

		public void setStrideId(String strideId) {
			this.strideId = strideId;
		}

		public String getStrideName() {
			return strideName;
		}

		public void setStrideName(String strideName) {
			this.strideName = strideName;
		}

		public List<RiskControl> getsR() {
			return sR;
		}

		public void setsR(List<RiskControl> sR) {
			this.sR = sR;
		}

	}
	
	public class RiskControl {
		private String id;
		private String name;
		private String family;
		private String familyId;
		private String description;
		
		public RiskControl(){}
		
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getFamily() {
			return family;
		}
		public void setFamily(String family) {
			this.family = family;
		}
		public String getFamilyId() {
			return familyId;
		}
		public void setFamilyId(String familyId) {
			this.familyId = familyId;
		}
		public String getDescription() {
			return description;
		}
		public void setDescription(String description) {
			this.description = description;
		}
		
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getCid() {
		return cid;
	}

	public void setCid(String cid) {
		this.cid = cid;
	}

	public List<RiskThreat> getRisks() {
		return risks;
	}

	public void setRisks(List<RiskThreat> risks) {
		this.risks = risks;
	}
}
