package eu.musaproject.slagenerator.entity;

public class JsonRiskAssessmentInput {

	private String id;
	private String family_name;
	private String family_id;
	private String control_id;
	private String control_name;
	
	public JsonRiskAssessmentInput(){};
	
	
	public JsonRiskAssessmentInput(String id, String family_name, String family_id, String control_id, String control_name) {
		super();
		this.id = id;
		this.family_name = family_name;
		this.family_id = family_id;
		this.control_id = control_id;
		this.control_name = control_name;
	}


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFamily_name() {
		return family_name;
	}

	public void setFamily_name(String family_name) {
		this.family_name = family_name;
	}

	public String getFamily_id() {
		return family_id;
	}

	public void setFamily_id(String family_id) {
		this.family_id = family_id;
	}

	public String getControl_id() {
		return control_id;
	}

	public void setControl_id(String control_id) {
		this.control_id = control_id;
	}

	public String getControl_name() {
		return control_name;
	}

	public void setControl_name(String control_name) {
		this.control_name = control_name;
	}


	
	
}
