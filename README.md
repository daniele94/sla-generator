# README - sla-generator-v2 #

La repository sla-generator è l'applicazione client-server che implementa le metodologie di modellazione delle minacce ed analisi del rischio.
L'applicazione è sviluppata utilizzando le seguenti tecnologie:
#### SERVER-SIDE ##
* Java
* Spring
* Spring-boot
* JPA - Hibernate
#### CLIENT-SIDE ##
* HTML, CSS (BOOTSTRAP)
* Javascript
* AngularJS
#### DATABASE ##
* MYSQL
* MONGODB

E' possibile comprendere la struttura del progetto e le varie tecnologie utilizzate andando alla pagina: 
http://websystique.com/spring-boot/spring-boot-angularjs-spring-data-jpa-crud-app-example/

### Guida alla configurazione ###
REQUISITI SOFTWARE 

* Java 8 
* Tomcat 7
* MongoDB
* Neo4J 
* MySQL Server

Per poter utilizzare ed avviare l'applicazione occorre:

1) Installare Java 8 ed impostare JRE_HOME come variabile d'ambiente

2) Estrarre Tomcat 7 e impostare CATALINA_HOME come variabile d'ambiente

3) Installare MongoDB e connettersi a localhost:27017

4) Mettere su un database MySql con i seguenti parametri:
    * Nome database: slagenerator
    * Username: root
    * Password: root
    * E' possibile modificare i parametri in ogni momento recandosi nel file application.properties
Nota: Bisogna creare un database vuoto denominato "slagenerator"

5) Scaricare in eclipse i seguenti plugin: 

    * Spring tool suite
    * Free Maker IDE from JBOSS
    
6) Avviare la repository "sla-model" con spring-boot per popolare il database statico

7) Scaricare il repository "sla-generator" come general project

8) Cliccare col tasto destro sul progetto -> configure -> convert to maven project

9) Avviare l'applicativo attraverso le opzioni: Run Us -> Spring Boot App

10) Aprire sul browser la pagina localhost:8080
NOTA: E' possibile modificare il numero di porta

### Contatti ###

* Daniele Granata, danielegranata94@gmail.com
