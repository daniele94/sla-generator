'use strict';

angular.module('slaGenerator').controller('SlaViewerController',
		['$sessionStorage', 'SlaViewerService', '$scope', function( $sessionStorage, SlaViewerService, $scope) {

			console.log("SlaViewerController called");

			var self = this;
			self.isArray = isArray;
			self.selectGeneratedSla = selectGeneratedSla;
			self.selectGeneratedAssessmentSla = selectGeneratedAssessmentSla;
			self.selectGeneratedComposedSla = selectGeneratedComposedSla;
			self.selectSlaFromRepo = selectSlaFromRepo;
			self.getSlaList = getSlaList;
			self.loadSlaContent = loadSlaContent;

			self.generatedSlaSelected = "active";
			self.generatedSlaAssessmentSelected = "null";
			self.generatedSlaComposedSelected = "null";
			self.slaFromRepoSelected = "null";
			self.slaList = null;

			self.slaViewerXml = null;
			self.slaFromRepoXml = null;
			self.slaFromRepoError = null;
			self.slaID = null;

			self.application = $sessionStorage.application;

//			if($sessionStorage.application.activeComponent.slaXml != null){
//				self.slaViewerXml = $sessionStorage.application.activeComponent.slaXml.replace(/<(\/?)([^:>\s]*:)?([^>]+)>/g, "<$1$3>");
//				console.log("self.slaViewerXml "+self.slaViewerXml);
//				self.slaID = $sessionStorage.application.activeComponent.slaId;
//				self.slaJson = $.xml2json($.parseXML(self.slaViewerXml));
//
//				self.context = self.slaJson.Context;
//
//				getCapabilitiesAndControlFramework();
//				getServiceLevelObjective();
//			}

			function getCapabilitiesAndControlFramework() {
				self.capabilities = self.slaJson.Terms.All.ServiceDescriptionTerm.serviceDescription.capabilities;
				if(isArray(self.capabilities.capability)){
					console.log("capabilities.capability e' array");
					self.controlFramework = self.capabilities.capability[0].controlFramework;
					self.controlFrameworkSize = isArray(self.capabilities.capability[0].controlFramework.NISTsecurityControl) ? (self.controlFramework.NISTsecurityControl.length) : 1;

				}
				else{
					console.log("capabilities.capability non e' array");
					self.controlFramework = self.capabilities.capability.controlFramework;
					self.controlFrameworkSize = isArray(self.capabilities.capability.controlFramework.NISTsecurityControl) ? (self.controlFramework.NISTsecurityControl.length) : 1;
				}	
			}

			function getServiceLevelObjective() {
				console.log("getServiceLevelObjective");
				if(isArray(self.slaJson.Terms.All.GuaranteeTerm)){
					self.sloList = self.slaJson.Terms.All.GuaranteeTerm[0].ServiceLevelObjective.CustomServiceLevel.objectiveList;
				}
				else{
					if(self.slaJson.Terms.All.GuaranteeTerm != null){
						self.sloList = self.slaJson.Terms.All.GuaranteeTerm.ServiceLevelObjective.CustomServiceLevel.objectiveList;
					}else{
						self.sloList = [];
					}
				}


				if(self.sloList != null){
					self.sloListSize = isArray(self.sloList.SLO) ? self.sloList.SLO.length : (self.sloList.SLO == null ? 0 : 1);
				}
				console.log("fine getServiceLevelObjective");
			}

			function isArray(object) {
				if(Object.prototype.toString
						.call(object) === '[object Array]') {

					return true;
				}
				return false;
			}

			function selectGeneratedSla(){
				self.generatedSlaSelected = "active";
				self.slaFromRepoSelected = "null";
				self.generatedSlaAssessmentSelected = "null";
				self.generatedSlaComposedSelected = "null";
				self.slaFromRepoXml = null;
				self.slaFromRepoError = null;

//				if($sessionStorage.application.activeComponent.slaXml != null){
//					self.slaViewerXml = $sessionStorage.application.activeComponent.slaXml.replace(/<(\/?)([^:>\s]*:)?([^>]+)>/g, "<$1$3>");
//					console.log("self.slaViewerXml "+self.slaViewerXml);
//					self.slaID = $sessionStorage.application.activeComponent.slaId;
//
//					self.slaJson = $.xml2json($.parseXML(self.slaViewerXml));
//
//					self.context = self.slaJson.Context;
//
//					getCapabilitiesAndControlFramework();
//					getServiceLevelObjective();
//				}else{
					if($sessionStorage.application.activeComponent.slaId != null){
						SlaViewerService.loadSlaContent($sessionStorage.application.activeComponent.slaId)
						.then(function (response) {
							console.log('Sla Content Get successfully'+response.data);
							if(response.data != null){
								self.slaID = $sessionStorage.application.activeComponent.slaId;
								$sessionStorage.application.activeComponent.slaXml = self.slaViewerXml;
								self.slaViewerXml = response.data.replace(/<(\/?)([^:>\s]*:)?([^>]+)>/g, "<$1$3>");
								console.log("self.slaViewerXml "+self.slaViewerXml);
								
								self.slaJson = $.xml2json($.parseXML(self.slaViewerXml));
								
								self.context = self.slaJson.Context;
								
								getCapabilitiesAndControlFramework();
								getServiceLevelObjective();
							}else{
								self.slaFromRepoXml = null;
								self.slaFromRepoError = 'Response from sla repo is null!';
							}
						},
						function (errResponse) {
							console.error('Error while getting Sla content');
							self.errorMessage = 'Error while getting Sla content: ' + errResponse.data.errorMessage;
							self.slaViewerXml = null;
							self.slaViewerXmlError = 'Error while getting Sla content: ' + errResponse.data.errorMessage;
						}
						);
					}else{
						self.slaViewerXml = null;
					}
//				}
			}

			function selectGeneratedAssessmentSla(){
				self.generatedSlaSelected = "null";
				self.generatedSlaAssessmentSelected = "active";
				self.generatedSlaComposedSelected = "null";
				self.slaFromRepoSelected = "null";
				self.slaFromRepoXml = null;
				self.slaFromRepoError = null;

//				if($sessionStorage.application.activeComponent.assessedSlaXml != null){
//					self.slaViewerXml = $sessionStorage.application.activeComponent.assessedSlaXml.replace(/<(\/?)([^:>\s]*:)?([^>]+)>/g, "<$1$3>");
//					console.log("self.slaViewerXml "+self.slaViewerXml);
//					self.slaID = $sessionStorage.application.activeComponent.assessedSlaId;
//
//					self.slaJson = $.xml2json($.parseXML(self.slaViewerXml));
//
//					self.context = self.slaJson.Context;
//
//					getCapabilitiesAndControlFramework();
//					getServiceLevelObjective();
//				}else{
					if($sessionStorage.application.activeComponent.assessedSlaId != null){
						SlaViewerService.loadSlaContent($sessionStorage.application.activeComponent.assessedSlaId)
						.then(function (response) {
							console.log('Sla Content Get successfully'+response.data);
							if(response.data != null){
								self.slaID = $sessionStorage.application.activeComponent.assessedSlaId;
								$sessionStorage.application.activeComponent.slaXml = self.slaViewerXml;
								self.slaViewerXml = response.data.replace(/<(\/?)([^:>\s]*:)?([^>]+)>/g, "<$1$3>");
								console.log("self.slaViewerXml "+self.slaViewerXml);

								self.slaJson = $.xml2json($.parseXML(self.slaViewerXml));

								self.context = self.slaJson.Context;

								getCapabilitiesAndControlFramework();
								getServiceLevelObjective();
							}else{
								self.slaFromRepoXml = null;
								self.slaFromRepoError = 'Response from sla repo is null!';
							}
						},
						function (errResponse) {
							console.error('Error while getting Sla content');
							self.errorMessage = 'Error while getting Sla content: ' + errResponse.data.errorMessage;
							self.slaViewerXml = null;
							self.slaViewerXmlError = 'Error while getting Sla content: ' + errResponse.data.errorMessage;
						}
						);
					}else{
						self.slaViewerXml = null;
					}
//				}
			}

			function selectGeneratedComposedSla(){
				self.generatedSlaSelected = "null";
				self.generatedSlaAssessmentSelected = "null";
				self.generatedSlaComposedSelected = "active";
				self.slaFromRepoSelected = "null";
				self.slaFromRepoXml = null;
				self.slaFromRepoError = null;

//				if($sessionStorage.application.activeComponent.composedSlaXml != null){
//					self.slaViewerXml = $sessionStorage.application.activeComponent.composedSlaXml.replace(/<(\/?)([^:>\s]*:)?([^>]+)>/g, "<$1$3>");
//					console.log("self.slaViewerXml "+self.slaViewerXml);
//					self.slaID = $sessionStorage.application.activeComponent.composedSlaId;
//
//					self.slaJson = $.xml2json($.parseXML(self.slaViewerXml));
//
//					self.context = self.slaJson.Context;
//
//					getCapabilitiesAndControlFramework();
//					getServiceLevelObjective();
//				}else{
					if($sessionStorage.application.activeComponent.composedSlaId != null){
						SlaViewerService.loadSlaContent($sessionStorage.application.activeComponent.composedSlaId)
						.then(function (response) {
							console.log('Sla Content Get successfully'+response.data);
							if(response.data != null){
								self.slaID = $sessionStorage.application.activeComponent.composedSlaId;
								$sessionStorage.application.activeComponent.slaXml = self.slaViewerXml;
								self.slaViewerXml = response.data.replace(/<(\/?)([^:>\s]*:)?([^>]+)>/g, "<$1$3>");
								console.log("self.slaViewerXml "+self.slaViewerXml);

								self.slaJson = $.xml2json($.parseXML(self.slaViewerXml));

								self.context = self.slaJson.Context;

								getCapabilitiesAndControlFramework();
								getServiceLevelObjective();
							}else{
								self.slaFromRepoXml = null;
								self.slaFromRepoError = 'Response from sla repo is null!';
							}
						},
						function (errResponse) {
							console.error('Error while getting Sla content');
							self.errorMessage = 'Error while getting Sla content: ' + errResponse.data.errorMessage;
							self.slaViewerXml = null;
							self.slaViewerXmlError = 'Error while getting Sla content: ' + errResponse.data.errorMessage;
						}
						);
					}else{
						self.slaViewerXml = null;
					}
//				}
			}

			function selectSlaFromRepo(){
				self.generatedSlaSelected = "null";
				self.slaFromRepoSelected = "active";
				self.generatedSlaAssessmentSelected = "null";
				self.generatedSlaComposedSelected = "null";
				self.slaViewerXml = null;
				self.getSlaList();
			}

			function getSlaList(){
				SlaViewerService.getSlaList()
				.then(function (response) {
					console.log('Sla List Get successfully'+response);
					self.slaList = $.xml2json($.parseXML(response));
					console.log("Sla list json: "+self.slaList.item[0]);
				},
				function (errResponse) {
					console.error('Error while getting Slas');
					self.errorMessage = 'Error while getting Slas: ' + errResponse.data.errorMessage;
					self.slaFromRepoXml = null;
				}
				);
			}

			function loadSlaContent(){
				SlaViewerService.loadSlaContent($scope.selectedSla)
				.then(function (response) {
					console.log('Sla Content Get successfully'+response.data);
					if(response.data != null){
						self.slaFromRepoError = null;
						self.slaFromRepoXml = response.data.replace(/<(\/?)([^:>\s]*:)?([^>]+)>/g, "<$1$3>");
						console.log("self.slaFromRepoXml "+self.slaFromRepoXml);
						self.slaID = $scope.selectedSla;

						self.slaJson = $.xml2json($.parseXML(self.slaFromRepoXml));

						self.context = self.slaJson.Context;

						getCapabilitiesAndControlFramework();
						getServiceLevelObjective();
					}else{
						self.slaFromRepoXml = null;
						self.slaFromRepoError = 'Response from sla repo is null!';
					}
				},
				function (errResponse) {
					console.error('Error while getting Sla content');
					self.errorMessage = 'Error while getting Sla content: ' + errResponse.data.errorMessage;
					self.slaFromRepoXml = null;
					self.slaFromRepoError = 'Error while getting Sla content: ' + errResponse.data.errorMessage;
				}
				);
			}
		}]);