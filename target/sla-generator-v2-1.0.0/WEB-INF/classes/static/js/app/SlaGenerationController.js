'use strict';

angular.module('slaGenerator').controller('SlaGenerationController',
		['$sessionStorage', 'SlaGenerationService', '$scope', '$location',  function( $sessionStorage, SlaGenerationService, $scope, $location) {

			console.log("SlaGenerationController called");

			var self = this;

			/* Session variables used:
			 * $sessionStorage.application // contains the application in use
			 */
			console.log("SlaGenerationController -> musa jwt token: "+$sessionStorage.musaJwtToken);
			console.log("AppSetupController -> musa user id: "+$sessionStorage.musaUserId);
			
			//Variables
			self.sourceOptions = [{ name: "Load SLOs from Risk Analysis", id: 1 }, { name: "Load SLOs from SLA Assessment", id: 2 }];
			self.selectedOption = $sessionStorage.selectedOption;

			self.application = $sessionStorage.application;

			//Functions
			self.getMetrics = getMetrics;
			self.getComponentMetrics = getComponentMetrics;
			self.generateSla = generateSla;
			self.storeSla = storeSla;
			self.setSelectedSLO = setSelectedSLO;

			if(self.application.activeComponent.slaXml != null){
				LoadXMLString('slaXmlString',self.application.activeComponent.slaXml);
			}

			function getMetrics(){
				getComponentMetrics();
			}

			function getComponentMetrics(){
				return SlaGenerationService.getComponentMetrics(self.application.activeComponent.id)
				.then(function (response) {
					console.log('Metrics get successfully '+JSON.stringify(response));
					$sessionStorage.application.activeComponent.metrics = response;
					self.application = $sessionStorage.application;
					LoadXMLString('slaXmlString',self.application.activeComponent.slaXml);
					alert("Metrics are successfully loaded!\nDefine your SLOs!");
				},
				function (errResponse) {
					console.error('Error while get Metrics ' + errResponse.data.message);
					self.errorMessage = 'Error while get Metrics: ' + errResponse.data.message;
					alert('Error while get Metrics!\n' + errResponse.data.message);
				}
				);
			}

			function generateSla(){
				return SlaGenerationService.generateSla(self.application.activeComponent.id, self.application.activeComponent.metrics)
				.then(function (response) {
					console.log('Sla generated successfully'+response.data);
					alert("Sla generated successfully'");
					$sessionStorage.application.activeComponent.slaXml = response.data;
					self.application = $sessionStorage.application;
					LoadXMLString('slaXmlString',self.application.activeComponent.slaXml);
				},
				function (errResponse) {
					console.error('Error while generating Sla');
					self.errorMessage = 'Error while generating Sla: ';
					alert('Error while generating Sla: ' + errResponse.data.message);
				}
				);
			}

			function storeSla(){
				console.log("store Sla called");
				return SlaGenerationService.storeSla(self.application.activeComponent.id, self.application.activeComponent.slaXml, false, $sessionStorage.musaJwtToken, $sessionStorage.musaUserId)
				.then(function (response) {
					console.log('Sla stored successfully');
					alert("Sla stored successfully with id "+response);
					self.application.activeComponent.slaId = response;
					self.application.activeComponent.state = "SLAT_GENERATED";

					for( var i = 0; i < self.application.components.length; i++){
						if(self.application.components[i].id == self.application.activeComponent.id &&
								self.application.components[i].kanbanId == self.application.activeComponent.kanbanId){
							self.application.components[i].slaId = response;
							self.application.components[i].state = "SLAT_GENERATED";
							break;
						}
					}

					$sessionStorage.application = self.application;
				},
				function (errResponse) {
					console.error('Error while storing Sla');
					self.errorMessage = 'Error while storing Sla: ' + errResponse.data.errorMessage;
				}
				);
			}

			function setSelectedSLO(){
				for(var i = 0 ; i < self.application.activeComponent.metrics.length; i++){
					if(self.application.activeComponent.metrics[i].metric.id == $scope.selectedSLO.id){
						self.application.activeComponent.metrics[i].selected = true;
						$scope.selectedSLO = null;
					}
				}
			}

		}]);