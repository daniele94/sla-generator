'use strict';

angular.module('slaGenerator').controller('AppViewController',
		['$sessionStorage', 'AppViewService', '$scope', '$location', '$state',  function( $sessionStorage, AppViewService, $scope, $location, $state) {

			console.log("AppViewController called");

			var self = this;

			//Variables
			self.application = $sessionStorage.application;
			self.selectedComponent;

			//Functions
			self.changeSelectedComponent = changeSelectedComponent;
			self.updateAppState = updateAppState;

			console.log(self.application);
			updateAppState();
			
			function changeSelectedComponent(){

				for( var i = 0; i < $sessionStorage.application.components.length; i++){
					if($sessionStorage.application.components[i].id == $sessionStorage.application.activeComponent.id
							&& $sessionStorage.application.components[i].kanbanId == $sessionStorage.application.activeComponent.kanbanId){
						$sessionStorage.application.components[i] = $sessionStorage.application.activeComponent;
						break;
					}
				}

				$sessionStorage.application.activeComponent = self.selectedComponent;
				self.application = $sessionStorage.application;
				$state.reload();
			}

			function updateAppState(){
				return AppViewService.updateAppState(self.application.id)
				.then(function (response) {
					console.log('App state get successfully '+JSON.stringify(response));
					$sessionStorage.application.state = response.data;
					self.application = $sessionStorage.application;
				},
				function (errResponse) {
					console.error('Error while get app state');
					self.errorMessage = 'Error while get Metrics: ' + errResponse.data.errorMessage;
				});
			}

		}]);