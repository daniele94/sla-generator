'use strict';

angular.module('slaGenerator').factory('SlaViewerService',
		['$sessionStorage','$localStorage', '$http', '$q', 'urls',
			function ($sessionStorage, $localStorage, $http, $q, urls) {

			var factory = {
					getSlaList: getSlaList,
					loadSlaContent: loadSlaContent,
			};

			return factory;
			
			function getSlaList() {

				var deferred = $q.defer();

				$http(
						{
							method : 'GET',
							url : urls.MANAGESLA_API+'/getAllSla'
						})
						.then(
								function successCallback(
										response) {
									deferred.resolve(response.data);
									console.log("Sla list: "+response.data);
									
								},
								function errorCallback(
										response) {
									console.error('Error while load Sla List : '+errResponse.data.errorMessage);
									deferred.reject(errResponse);
								});

				return deferred.promise;
			}
			
			function loadSlaContent(slaId) {

				var deferred = $q.defer();

				$http(
						{
							method : 'GET',
							url : urls.MANAGESLA_API+'/getSlaContent?slaId='+slaId
						})
						.then(
								function successCallback(
										response) {
									deferred.resolve(response.data);
									console.log("Sla content: "+response.data);
									
								},
								function errorCallback(
										response) {
									console.error('Error while load Sla content : '+errResponse.data.errorMessage);
									deferred.reject(errResponse);
								});

				return deferred.promise;
			}
		}
		]);