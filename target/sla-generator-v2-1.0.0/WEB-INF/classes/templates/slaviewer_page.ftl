
<div class=container>
	<div class="panel panel-default">
		<div class="panel-heading text-center">
			<span class="lead">
				<h2>SLA Quick Viewer
					({{ctrl.application.activeComponent.name}})</h2>
			</span>
		</div>

		<div class="panel-body">


			<ul class="nav nav-pills nav-justified">
				<li role="presentation" class="{{ctrl.generatedSlaSelected}}"><a
					ng-click="ctrl.selectGeneratedSla()">Show Generated SLA</a></li>
				<li role="presentation"
					class="{{ctrl.generatedSlaAssessmentSelected}}"><a
					ng-click="ctrl.selectGeneratedAssessmentSla()">Show Assessed
						SLA</a></li>
						<li role="presentation"
					class="{{ctrl.generatedSlaComposedSelected}}"><a
					ng-click="ctrl.selectGeneratedComposedSla()">Show Composed
						SLA</a></li>
				<li role="presentation" class="{{ctrl.slaFromRepoSelected}}"><a
					ng-click="ctrl.selectSlaFromRepo()">Select SLA From Repository</a></li>
			</ul>
			<br>
			<div ng-show="ctrl.slaFromRepoSelected == 'active'">
				<div class="form-group">
					<label for="sel1">Choose SLA Identifier:</label> <select
						class="form-control"
						ng-options="sla.id as sla.id for sla in ctrl.slaList.item track by sla.id"
						ng-model="selectedSla" ng-change="ctrl.loadSlaContent()"></select>
				</div>
			</div>

			<div ng-show="ctrl.slaFromRepoError">
				<br>
				<h4>Error: {{ctrl.slaFromRepoError}}</h4>
				<br>
			</div>

			<div class="tab-pane" id="tabStoreParams"
				ng-show="ctrl.slaViewerXml != null || ctrl.slaFromRepoXml != null">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-target="#collapse0">Context</a> <span
								class="label label-primary label-as-badge">1</span>
						</h4>
					</div>
					<div id="collapse0" class="panel-collapse collapse">
						<div class="panel-body">

							<table class="table table-striped " style="width: 100%">
								<thead>
									<tr>
										<td style="width: 15%;"><b>AgreementInitiator</b></td>
										<td style="width: 2%;"></td>
										<td style="width: 15%;"><b>AgreementResponder</b></td>
										<td style="width: 2%;"></td>
										<td style="width: 15%;"><b>ServiceProvider</b></td>
										<td style="width: 2%;"></td>
										<td style="width: 15%;"><b>TemplateName</b></td>
										<td style="width: 2%;"></td>
										<td style="width: 15%;"><b>SLA id</b></td>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td style="width: 15%;">{{ctrl.context.AgreementInitiator}}</td>
										<td style="width: 2%;"></td>
										<td style="width: 15%;">{{ctrl.context.AgreementResponder}}</td>
										<td style="width: 2%;"></td>
										<td style="width: 15%;">{{ctrl.context.ServiceProvide}}</td>
										<td style="width: 2%;"></td>
										<td style="width: 15%;">{{ctrl.context.TemplateName}}</td>
										<td style="width: 2%;"></td>
										<td style="width: 15%;">{{ctrl.slaID}}</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>

				</div>


				<!--  SERVICE DESCRIPTION TERM - CONTROL FRAMEWORK E SECURITY CONTROL  -->
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-target="#collapse3">Service
								Description Term</a> <span
								class="label label-primary label-as-badge">{{ctrl.controlFrameworkSize}}</span>
						</h4>
					</div>
					<div id="collapse3" class="panel-collapse collapse">
						<div class="panel-body ">

							<table class="table table-striped " style="width: 100%">
								<thead>
									<tr>
										<td style="width: 20%;"><b>Control Framework</b></td>
										<td style="width: 5%;"></td>
										<td style="width: 85%;"><b>Name</b></td>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td style="width: 20%;">{{ctrl.controlFramework.id}}</td>
										<td style="width: 5%;"></td>
										<td style="width: 85%;">{{ctrl.controlFramework.frameworkName}}</td>
									</tr>
								</tbody>
							</table>

							<br />
							<table class="table table-striped" style="width: 100%;">
								<thead>
									<tr>
										<td style="width: 20%;"><b>Control Family</b></td>
										<td style="width: 5%;"></td>
										<td style="width: 10%;"><b>ID</b></td>
										<td style="width: 5%;"></td>
										<td style="width: 70%;"><b>Name</b></td>
									</tr>
								</thead>
								<tbody>
									<tr
										ng-if="ctrl.isArray(ctrl.controlFramework.NISTsecurityControl)"
										ng-repeat="item in ctrl.controlFramework.NISTsecurityControl | orderBy:'+control_family'">

										<td style="width: 20%;">{{item.control_family}}</td>
										<td style="width: 5%;"></td>
										<td style="width: 10%;">{{item.id}}</td>
										<td style="width: 5%;"></td>
										<td style="width: 70%;">{{item.name}}</td>
									</tr>
									<tr
										ng-if="!ctrl.isArray(ctrl.controlFramework.NISTsecurityControl)">

										<td style="width: 20%;">{{controlFramework.NISTsecurityControl.control_family}}</td>
										<td style="width: 5%;"></td>
										<td style="width: 10%;">{{controlFramework.NISTsecurityControl.id}}</td>
										<td style="width: 5%;"></td>
										<td style="width: 70%;">{{controlFramework.NISTsecurityControl.name}}</td>
									</tr>


									<!--  CCM  
								<tr ng-if="Array.isArray(controlFramework.CCMsecurityControl)"
									ng-repeat="item in controlFramework.CCMsecurityControl">

									<td style="width: 20%;">{{item.control_family}}</td>
									<td style="width: 5%;"></td>
									<td style="width: 10%;">{{item.id}}</td>
									<td style="width: 5%;"></td>
									<td style="width: 70%;">{{item.name}}</td>
								</tr>
								<tr ng-if="controlFramework.CCMsecurityControl && !Array.isArray(controlFramework.CCMsecurityControl)">
															  
									<td style="width: 20%;">{{controlFramework.CCMsecurityControl.control_family}}</td>
									<td style="width: 5%;"></td>
									<td style="width: 10%;">{{controlFramework.CCMsecurityControl.id}}</td>
									<td style="width: 5%;"></td>
									<td style="width: 70%;">{{controlFramework.CCMsecurityControl.name}}</td>
								</tr>
								-->
								</tbody>
							</table>

						</div>
					</div>

				</div>


				<!--  SERVICE LEVEL OBJECTIVES -->
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-target="#collapse2">Service
								Level Objectives</a> <span
								class="label label-primary label-as-badge">{{ctrl.sloListSize}}</span>
						</h4>
					</div>
					<div id="collapse2" class="panel-collapse collapse">
						<div class="panel-body ">


							<table class="table table-striped" style="width: 100%;">
								<thead>
									<tr>
										<td style="width: 10%;"><b>ID</b></td>
										<td style="width: 5%;"></td>
										<td style="width: 50%;"><b>Metric Ref</b></td>
										<td style="width: 5%;"></td>
										<td style="width: 20%;"><b>Operator</b></td>
										<td style="width: 5%;"></td>
										<td style="width: 20%;"><b>Operand</b></td>
									</tr>
								</thead>
								<tbody>
									<tr ng-if="ctrl.isArray(ctrl.sloList.SLO)"
										ng-repeat="item in ctrl.sloList.SLO">

										<td style="width: 10%;">{{item.SLO_ID}}</td>
										<td style="width: 5%;"></td>
										<td style="width: 50%;">{{item.MetricREF}}</td>
										<td style="width: 5%;"></td>
										<td style="width: 20%;">{{item.SLOexpression.oneOpExpression.operator}}</td>
										<td style="width: 5%;"></td>
										<td style="width: 20%;">{{item.SLOexpression.oneOpExpression.operand}}</td>
									</tr>
									<tr ng-if="!ctrl.isArray(ctrl.sloList.SLO)">

										<td style="width: 10%;">{{ctrl.sloList.SLO.SLO_ID}}</td>
										<td style="width: 5%;"></td>
										<td style="width: 50%;">{{ctrl.sloList.SLO.MetricREF}}</td>
										<td style="width: 5%;"></td>
										<td style="width: 20%;">{{ctrl.sloList.SLO.SLOexpression.oneOpExpression.operator}}</td>
										<td style="width: 5%;"></td>
										<td style="width: 20%;">{{ctrl.sloList.SLO.SLOexpression.oneOpExpression.operand}}</td>
									</tr>
								</tbody>
							</table>

						</div>
					</div>

				</div>
			</div>