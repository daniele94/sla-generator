<style>
a.tip {
	text-decoration: none
}

a.tip:hover {
	cursor: help;
	position: relative
}

a.tip span {
	display: none
}

a.tip:hover span {
	border: #c0c0c0 1px dotted;
	padding: 5px 20px 5px 5px;
	display: block;
	z-index: 100;
	background: #f0f0f0 no-repeat 100% 5%;
	left: 0px;
	margin: 10px;
	width: 250px;
	position: absolute;
	top: 10px;
	text-decoration: none
}
</style>

<div class=container>
	<div class="panel panel-default">
		<div class="panel-heading text-center">
			<span class="lead">
				<h2>SLA Generation ({{ctrl.application.activeComponent.name}})</h2>
				
			</span>
		</div>

		<div class="panel-body">
			<div
				ng-show="ctrl.application.id == null || ctrl.application.id == ''">
				There isn't an application to execute Sla Generation.<br> To
				start with the SLA generation select "SetUp Application" button on
				top right of header bar and load an application.
			</div>

			<div
				ng-show="ctrl.application.id != null && ctrl.application.id != ''">

				<p>To execute the SLA generation:</p>
				<li>Click the button "LOAD Metrics"</li>
				<li>Choose the Metric you want to add and then click "Define SLO"
					button</li>
				<li>Insert a value for the added Metric to define the related
					SLO</li>
				<li>Click the "Generate SLA" button</li> <br>
				<p>
					<b>NB:</b> To save the generated SLA do not forget to click the
					"Store SLA" button after the generation!
				</p>

				<button style="text-align: center;" class="btn btn-primary"
					ng-click="ctrl.getMetrics()" ng-disabled="ctrl.application.activeComponent.componentGroup!='saas' && ctrl.application.activeComponent.componentGroup!='SaaS'">LOAD Metrics</button>

			</div>
			<br>
			<div class="panel panel-default">
				<div class="panel-heading text-center">
					<h2>Select the Metrics for SLO definition</h2>
				</div>
				<div class="panel-body">
					<div
						ng-show="ctrl.application.activeComponent.metrics == null || !(ctrl.application.activeComponent.metrics| filter:nameText).length">
						No metrics found for the component, or the component is not setted
						up!</div>

					<div class="form-group"
						ng-show="ctrl.application.activeComponent.metrics != null && (ctrl.application.activeComponent.metrics| filter:nameText).length">
						<label for="sel1">Choose Metric to add:</label> <select
							class="form-control"
							ng-options="metric.metric as metric.metric.metricName for metric in ctrl.application.activeComponent.metrics track by metric.metric.metricName"
							ng-model="selectedSLO"></select>
					</div>

					<div ng-show=selectedSLO>
						<table border="1" style="width: 100%;">
							<tr>

								<td style="padding: 10px;" width="20%"><b>{{selectedSLO.metricName}}</b></td>
								<td style="padding: 10px;" width="100%"><b>Description:</b><br>{{selectedSLO.metricDescription}}
									<br> <br> <b>Input Type: </b>{{selectedSLO.unit}} <b>
										Input Range: </b>{{selectedSLO.valueRange}} <br></td>
								<td style="padding: 10px;" width="20%"><button
										class="btn btn-primary" ng-click="ctrl.setSelectedSLO()">Define SLO</button></td>
							</tr>
						</table>
					</div>
					<br>
					<div class="panel panel-default"
						ng-show="ctrl.application.activeComponent.metrics != null && (ctrl.application.activeComponent.metrics| filter:nameText).length">
						<div class="panel-heading text-center">
							<h2>Selected SLOs</h2>
						</div>
						<div class="panel-body">
							<div class="table-responsive"
								ng-show="ctrl.application.activeComponent.metrics != null">
								<table class="table table-hover" style="border-bottom: 1px solid #ddd"
									ng-repeat="componentMetric in ctrl.application.activeComponent.metrics | filter: { selected: 'true' }">
									<tbody>
										<tr>
											<td width="100%">
												<!-- tabella tipo1 (yes/no) -->
												<table border="1" style="width: 100%;">
													<tr>

														<td style="padding: 10px;" width="2%"><input
															type="checkbox" ng-model="componentMetric.selected"></td>
														<td style="padding: 10px;" width="15%"><b>{{componentMetric.metric.metricName}}</b></td>
														<td style="padding: 10px;" width="58%"><b>Description:</b><br>{{componentMetric.metric.metricDescription}}
															<br> <br> <b>Input Type: </b>{{componentMetric.metric.unit}}
															<b> Input Range: </b>{{componentMetric.metric.valueRange}}
															<br> <b>Value = </b><input
															ng-model="componentMetric.value"></td>
															
															<td style="padding: 10px;" width="25%"><b>Associated Controls:</b><ul style=" padding-left: 0; margin-left: 0;"><li ng-repeat="control in componentMetric.associatedControls">
															
															<a class="tip">{{control.controlName}}<span>{{control.controlDescription}}</span></a>
															
															</li></ul></td>


													</tr>
												</table>

											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="panel panel-default">
				<div class="panel-heading text-center">
					<h2>Available Options</h2>
				</div>
				<div class="panel-body">
					<table style="width: 100%;">
						<tbody>
							<tr>
								<td style="width: 5%;"></td>
								<td style="width: 30%;"><button style="width: 90%"
										class="btn btn-primary"
										ng-disabled="ctrl.application.activeComponent.metrics == null"
										ng-click="ctrl.generateSla()">Generate SLA</button></td>
								<td style="width: 30%;"><button style="width: 90%"
										class="btn btn-primary"
										ng-disabled="ctrl.application.activeComponent.slaXml == null"
										ng-click="ctrl.storeSla()">Store SLA</button></td>
								<td style="width: 30%;"><button style="width: 90%"
										class="btn btn-primary"
										ng-disabled="ctrl.application.activeComponent.slaXml == null"
										ui-sref="slaviewer">Show SLA</button></td>
								<td style="width: 5%;"></td>
							</tr>
						</tbody>
					</table>

				</div>
			</div>

			<div class="panel panel-default">
				<div class="panel-heading text-center">
					<h2>Generated SLA</h2>
				</div>

				<div class="panel-body">
					<div ng-show="ctrl.application.activeComponent.slaXml == null">No SLA
						XML found for the component, or the component is not set up!</div>
					<div class="table-responsive"
						ng-show="ctrl.application.activeComponent.slaXml != null">
						<div class="well" style="overflow-y: auto; height: 450px;"
							id='slaXmlString'></div>
					</div>
				</div>
			</div>

		</div>