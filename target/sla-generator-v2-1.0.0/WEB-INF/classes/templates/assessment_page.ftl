<div class=container>
	<div class="panel panel-default">
		<div class="panel-heading text-center">
			<span class="lead">
				<h2>SLA Assessment ({{ctrl.application.activeComponent.name}})</h2>
			</span>
		</div>

		<div class="panel-body">
			<div
				ng-show="ctrl.application.id == null || ctrl.application.id == ''">
				There isn't an application to execute Sla Generation.<br> To
				start with the SLA generation select "SetUp Application" button on
				top right of header bar and load an application.
			</div>

			<div
				ng-show="ctrl.application.id != null && ctrl.application.id != ''">

				<p>To execute the SLA assessment:</p>
				<li>Click the button "LOAD Questions"</li>
				<li>Answer the listed questions for each Control</li>
				<li>Click "Submit and Validate Questionnaire"</li>
				<li>Click the "Generate Assessed SLA" button</li> <br>
				<p>
					<b>NB:</b> To save the generated SLA do not forget to click the
					"Store SLA" button after the generation!
				</p>

				<button style="text-align: center;" class="btn btn-primary"
					ng-click="ctrl.getComponentQuestions()"
					ng-disabled="ctrl.application.activeComponent.componentGroup!='saas' && ctrl.application.activeComponent.componentGroup!='SaaS'">LOAD
					Questions and Previous Replies</button>

				<br></br>

				<p>Is your component an Agent?</p>
				<li>Click the button "LOAD Control Families"</li>
				<li>Choose one or more control families</li>
				<li>Submit them and so Answer the listed questions for each
					Control</li>
				<p></p>
				<button style="text-align: center;" class="btn btn-primary"
					ng-click="ctrl.getControlFamilies()"
					ng-disabled="ctrl.application.activeComponent.componentGroup!='saas' && ctrl.application.activeComponent.componentGroup!='SaaS'">LOAD
					Control Families</button>

			</div>

			<br>
			<div class="panel panel-default"
				ng-show="ctrl.controlFamilies.length > 0">
				<div class="panel-heading text-center">
					<h2>Select the Control Families</h2>
				</div>
				<div class="panel-body">

					<div class="form-group">
						<label for="sel1">Choose Control Family to add:</label> <select
							class="form-control"
							ng-options="controlFamily for controlFamily in ctrl.controlFamilies"
							ng-model="selectedControlFamily"></select>
					</div>

					<div ng-show=selectedControlFamily>
						<table border="1" style="width: 100%;">
							<tr>

								<td style="padding: 10px;" width="20%"><b>{{selectedControlFamily}}</b></td>

								<td style="padding: 10px;" width="20%"><button
										class="btn btn-primary"
										ng-click="ctrl.addSelectedControlFamily()">Add</button></td>
							</tr>
						</table>
					</div>

					<div class="panel panel-default"
						ng-show="ctrl.selectedControlFamilies.length > 0">
						<div class="panel-heading text-center">
							<h2>Selected Control Families</h2>
						</div>
						<div class="panel-body">
							<div class="table-responsive">
								<table class="table table-hover"
									style="border-bottom: 1px solid #ddd"
									ng-repeat="controlFamily in ctrl.selectedControlFamilies">
									<tbody>
										<tr>
											<td width="100%">
												<!-- tabella tipo1 (yes/no) -->
												<table border="1" style="width: 100%;">
													<tr>


														<td style="padding: 10px;" width="15%"><b>{{controlFamily}}</b></td>



													</tr>
												</table>

											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						
						
						<table style="width: 100%;" ng-show="ctrl.selectedControlFamilies.length > 0">
						<tbody>
							<tr>
								<td style="width: 30%;"></td>
								<td style="width: 30%;"><button style="width: 90%"
										class="btn btn-primary" ng-click="ctrl.submitControlFamilies()">Submit
										Control Families</button></td>
								<td style="width: 30%;"></td>
							</tr>
						</tbody>
					</table>
					</div>
				</div>
			</div>

			<br>

			<div class="panel panel-default">
				<div class="panel-heading text-center">
					<h2>Security Reviews</h2>
				</div>
				<div class="panel-body">

					<div class="table-responsive">
						<table class=table>
							<thead class=active>
								<tr>
									<th style="padding: 10px;" width="70%">Control Info</th>
									<th style="padding: 10px;" width="30%" class=text-center>Positive
										Answers</th>
								</tr>
							</thead>
							<tbody>
								<table class="table table-hover">
									<tbody>
										<tr
											ng-repeat="control in ctrl.activeComponentQuestions  | orderBy:'+control.controlId'">
											<td width="100%">
												<div>
													<table style="width: 100%;">
														<tr>

															<td width=70% class=text-left><b>Control id:</b>
																{{control.control.controlId}} <br> <b>Control
																	name:</b> {{control.control.controlName}} <br> <b>Family
																	id:</b> {{control.control.familyId}} <br> <b>Family
																	name:</b> {{control.control.familyName}}</td>
															<td width=30% class=text-center><h4>
																	<span
																		ng-class="( ((control.controlQuestions | filter : {answer: 'Yes'} : true).length) + 
																				((control.controlQuestions | filter : {answer: 'Not Applicable'} : true).length) )  == 
																				control.controlQuestions.length  ? 
																	'label label-success' : 
																	((control.controlQuestions | filter : {answer: '' } : false).length == control.controlQuestions.length ? 'label label-warning' : 'label label-danger')">

																		{{ (control.controlQuestions | filter : { answer: '' }
																		: false).length }}/{{control.controlQuestions.length}}</span>
																</h4></td>

															<td>
																<button class="btn btn-primary"
																	ng-disabled="{{control.controlQuestions.length}} == 0"
																	ng-click="control.collapsed = !control.collapsed"
																	ng-show="!control.collapsed">Show
																	Questionnaire</button>
																<button class="btn btn-primary"
																	ng-disabled="{{control.controlQuestions.length}} == 0"
																	ng-click="control.collapsed = !control.collapsed"
																	ng-show="control.collapsed">Hide Questionnaire</button>
															</td>
														</tr>
													</table>
													<br>
													<div ng-show="control.collapsed">
														<table border="1">
															<thead>
																<tr>
																	<th style="padding: 10px;" width="30%">Source Info</th>
																	<th style="padding: 10px;" width="50%"
																		class=text-center>Question</th>
																	<th style="padding: 10px;" width="20%"
																		class=text-center>Answer</th>
																</tr>
															</thead>
															<tbody>

																<tr ng-repeat="question in control.controlQuestions">

																	<td style="padding: 10px;" width="30%"><b>Source
																			id:</b>{{question.questionControl.sourceId}} <br> <b>Source
																			name:</b>{{question.questionControl.sourceName}}</td>
																	<td style="padding: 10px;" width="50%">{{question.questionControl.description}}</td>
																	<td style="padding: 10px;" width="20%">
																	<select
																		class="form-control" ng-model="question.answer" ng-change="ctrl.updateQuestionsAnswer(question)">
																			<option value="No">No</option>
																			<option value="Not Applicable">Not
																				Applicable</option>
																			<option value="Yes">Yes</option>
																	</select></td>
																</tr>

															</tbody>


														</table>
														<br>
													</div>
												</div>

											</td>
										</tr>
									<tbody>
								</table>

							</tbody>
						</table>
					</div>
				</div>
			</div>

			<div class="panel panel-default">
				<div class="panel-heading text-center">
					<h2>Available Options</h2>
				</div>
				<div class="panel-body">
					<table style="width: 100%;">
						<tbody>
							<tr>
								<td style="width: 30%;"></td>
								<td style="width: 30%;"><button style="width: 90%"
										class="btn btn-primary" ng-click="ctrl.submitQuestionnaire()"
										ng-disabled="ctrl.application.activeComponent.componentGroup!='saas' && ctrl.application.activeComponent.componentGroup!='SaaS'">Submit
										and Validate Questionnaire</button></td>
								<td style="width: 30%;"></td>
							</tr>
						</tbody>
					</table>

				</div>

			</div>

			<div class="panel panel-default"
				ng-show="ctrl.application.activeComponent.assessedMetrics != null && (ctrl.application.activeComponent.assessedMetrics| filter:nameText).length">
				<div class="panel-heading text-center">
					<h2>SLOs Review</h2>
				</div>
				<div class="panel-body">
					<div class="table-responsive"
						ng-show="ctrl.application.activeComponent.assessedMetrics != null">
						<table class="table table-hover"
							ng-repeat="componentMetric in ctrl.application.activeComponent.assessedMetrics | filter: { selected: 'true' }">
							<tbody>
								<tr>
									<td width="100%">
										<!-- tabella tipo1 (yes/no) -->
										<table border="1" style="width: 100%;">

											<tr
												ng-style="{'background-color':(componentMetric.assessedTrue?'#cdc':'#ddcccc')}">
												<td style="padding: 10px;" width="10%"><input
													type="checkbox" ng-model="componentMetric.assessedSelected"></td>
												<td style="padding: 10px;" width="20%"><b>{{componentMetric.metric.metricName}}</b></td>
												<td style="padding: 10px;" width="100%"><b>Description:</b><br>{{componentMetric.metric.metricDescription}}
													<br> <br> <b>Input Type: </b>{{componentMetric.metric.unit}}
													<b> Input Range: </b>{{componentMetric.metric.valueRange}}
													<br> <b>Value = </b><input 
													ng-model="componentMetric.value"></td>


											</tr>
										</table>

									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>

			<div class="panel panel-default">
				<div class="panel-heading text-center">
					<h2>Available Options</h2>
				</div>
				<div class="panel-body">
					<table style="width: 100%;">
						<tbody>
							<tr>
								<td style="width: 5%;"></td>
								<td style="width: 30%;"><button style="width: 90%"
										class="btn btn-primary"
										ng-disabled="ctrl.application.activeComponent.assessedMetrics == null"
										ng-click="ctrl.generateSla()">Generate Assessed SLA</button></td>
								<td style="width: 30%;"><button style="width: 90%"
										class="btn btn-primary"
										ng-disabled="ctrl.application.activeComponent.assessedSlaXml == null"
										ng-click="ctrl.storeSla()">Store Assessed SLA</button></td>
								<td style="width: 30%;"><button style="width: 90%"
										class="btn btn-primary"
										ng-disabled="ctrl.application.activeComponent.assessedSlaXml == null"
										ui-sref="slaviewer">Show Assessed SLA</button></td>
								<td style="width: 5%;"></td>
							</tr>
						</tbody>
					</table>

				</div>
			</div>

			<div class="panel panel-default">
				<div class="panel-heading text-center">
					<h2>Generated Assessed SLA</h2>
				</div>

				<div class="panel-body">
					<div
						ng-show="ctrl.application.activeComponent.assessedSlaXml == null">No
						assessed SLA XML found for the component, or the component is not
						set up!</div>
					<div class="table-responsive"
						ng-show="ctrl.application.activeComponent.assessedSlaXml != null">
						<div class="well" style="overflow-y: auto; height: 450px;"
							id='slaXmlString'></div>
					</div>
				</div>
			</div>

		</div>

	</div>
</div>