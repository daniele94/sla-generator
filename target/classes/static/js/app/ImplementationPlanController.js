'use strict';

angular.module('slaGenerator').controller('ImplementationPlanController',
		['$sessionStorage','$scope', '$state', '$location', function( $sessionStorage,  $scope, $state, $location) {

			var self = this;

		    var results = {}; 
//			var graphId;
//			var tableId;
//			var renderGraph;
//			//Global Variables
//			self.cname;
//			self.cspname;
//
			//Functions
			self.displayFileContents = displayFileContents;

			//initialization
//			init();
			
			
			document.getElementById('input-plan')
			.addEventListener('change',
					displayFileContents, false);
			
			
			function displayFileContents(e) {
				console.log("displayFileContents")
				var file = e.target.files[0];
				
				if (!file) {
					console.log("!file")
					return;
				}
				var reader = new FileReader();

				reader.readAsText(file);

				reader.onload = function(e) {
					console.log("onload")
					var contents = e.target.result;
					$scope.results = JSON.parse(contents);
					console.log($scope.results)
					console.log("-----")
					console.log(JSON.parse(contents))
					$scope.$apply();
				};

			}
			


//			function init() {
//				//todo dynamic configuration
//				var config = {}
//				//var connection = function() { return {url:$("#neo4jUrl").val(), user:$("#neo4jUser").val(),pass:$("#neo4jPass").val()}; }
//				var connection = function() {
//					return {
//						url : "http://localhost:7474",
//						user : "neo4j",
//						pass : "max"
//					};
//				}
//				//new Cy2NeoD3(config, "graph", "datatable", "cypher","execute", connection, true);
//			    neod3 = new Neod3Renderer();
//				neo = new Neo(connection);
//				graphId="graph";
//				tableId="datatable";
//				renderGraph=true;
//				
//			}
			
			
			
			

			
			// Read Cypher from Input and call ComposeService with model
//			document.getElementById('input-cypher')
//			.addEventListener('change',
//					readCypherFile, false);
//
//			function readCypherFile(e) {
//				var file = e.target.files[0];
//				if (!file) {
//					return;
//				}
//				var reader = new FileReader();
//
//				reader.readAsText(file);
//
//				reader.onload = function(e) {
//					var contents = e.target.result;
//					$scope.CypherContent = contents;
////					$scope.$apply();
//					
//					ComposeService.setupGraphfromCypher($scope.CypherContent)
//					.then(function (response) {
//						execute();
//					}
//					);
//					
//				};
//
//			}

//			document.getElementById('input-sla')
//			.addEventListener('change',
//					readSLAFile, false);
//
//			function readSLAFile(e) {
//				var file = e.target.files[0];
//				if (!file) {
//					return;
//				}
//				var reader = new FileReader();
//
//				reader.readAsText(file);
//
//				reader.onload = function(e) {
//					var contents = e.target.result;
//					$scope.CypherContent = contents;
////					$scope.$apply();
//					
//					ComposeService.setupSLA($scope.CypherContent,self.cspname)
//					.then(function (response) {
//						execute();
//					}
//					);
//					
//				};
//
//			}

//			document.getElementById('input-slat')
//			.addEventListener('change',
//					readSLATFile, false);
//
//			function readSLATFile(e) {
//				var file = e.target.files[0];
//				if (!file) {
//					return;
//				}
//				var reader = new FileReader();
//
//				reader.readAsText(file);
//
//				reader.onload = function(e) {
//					var contents = e.target.result;
//					$scope.CypherContent = contents;
////					$scope.$apply();
//					
//					ComposeService.setupSLAT($scope.CypherContent,self.cname)
//					.then(function (response) {
//						execute();
//					}
//					);
//					
//				};
//
//			}

//			function compose() {
//				ComposeService.compose()
//				.then(function (response) {
//					execute();
//				}
//				);
//			}
			
//			function execute() {
//				try {
//		//			evt.preventDefault();
//					//var query = editor.getValue();
////					var query="match (n:service), (sla:SLA), (csp:CSP) return csp,n,sla";
////					var query="match (a)-[p:provides]->(b), (c)-[h:hosts]->(d), (e)-[u:uses]->(f), (k)-[g:grants]->(j), (t)-[s:supports]->(q) return p,h,u,g,s";
//					var query="match (n)-[r]->(b) return n,r,b"
//					console.log(query);
//					console.log("Executing Query",query);
//					var execButton = $(this).find('i');
//					execButton.toggleClass('fa-play-circle-o fa-spinner fa-spin')
//					neo.executeQuery(query,{},function(err,res) {
//						execButton.toggleClass('fa-spinner fa-spin fa-play-circle-o')
//						res = res || {}
//						var graph=res.graph;
//						if (renderGraph) {
//							if (graph) {
//								var c=$("#"+graphId);
//								c.empty();
//								neod3.render(graphId, c ,graph);
//								renderResult(tableId, res.table);
//							} else {
//								if (err) {
//									console.log(err);
//									if (err.length > 0) {
//										sweetAlert("Cypher error", err[0].code + "\n" + err[0].message, "error");
//									} else {
//										sweetAlert("Ajax " + err.statusText, "Status " + err.status + ": " + err.state(), "error");
//									}
//								}
//							}
//						}
//						if(cbResult) cbResult(res);
//					});
//				} catch(e) {
//					console.log(e);
//					sweetAlert("Catched error", e, "error");
//				}
//			}

		}]);