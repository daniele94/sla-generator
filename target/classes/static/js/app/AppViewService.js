'use strict';

angular.module('slaGenerator').factory('AppViewService',
		['$sessionStorage','$localStorage', '$http', '$q', 'urls',
			function ($sessionStorage, $localStorage, $http, $q, urls) {

			var factory = {
					setUpComponentById: setUpComponentById,
					updateAppState: updateAppState

			};

			return factory;

			function setUpComponentById(appId, componentId, source){
				var deferred = $q.defer();

				$http(
						{
							method : 'POST',
							url : urls.APPSETUP+"/componentFromId",
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							transformRequest : function(
									obj) {
								var str = [];
								for ( var p in obj)
									str
									.push(encodeURIComponent(p)
											+ "="
											+ encodeURIComponent(obj[p]));
								return str.join("&");
							},
							data : {
								'appId' : appId,
								'componentId' : componentId,
								'source' : source,
							}

						})
						.then(
								function successCallback(
										response) {
									deferred.resolve(response.data);
								},
								function errorCallback(
										errResponse) {
									deferred.reject(errResponse);
								});

				return deferred.promise;
			}

			function updateAppState(appId){
				var deferred = $q.defer();

				$http(
						{
							method : 'GET',
							url : urls.APPSETUP+"/applications/state?appId="+appId
						})
						.then(
								function successCallback(
										response) {
									deferred.resolve(response.data);
								},
								function errorCallback(
										errResponse) {
									deferred.reject(errResponse);
								});

				return deferred.promise;
			}

		}
		]);