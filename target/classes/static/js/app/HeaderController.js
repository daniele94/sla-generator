'use strict';

angular.module('slaGenerator').controller('HeaderController',
		['$sessionStorage', 'HeaderService', '$scope', '$state', '$location', function( $sessionStorage, HeaderService, $scope, $state, $location) {

			var self = this;

			//Global Variables
			var musaToken = "";
			var musaUserId = "";
			if(window.localStorage["musa.sso.token"] == null || window.localStorage["musa.sso.token"] == ""
				|| window.localStorage["musa.sso.profile"] == null || window.localStorage["musa.sso.profile"] == ""){
				console.log("musa.sso.token is null. Set it manually!")
				console.log("musa.sso.profile is null. Set it manually!")
				musaToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJlbWFpbCI6InAuZGVyb3NhODdAZ21haWwuY29tIiwiZW1haWxfdmVyaWZpZWQiOnRydWUsImlzcyI6Imh0dHBzOi8vbXVzYS1wcm9qZWN0LmV1LmF1dGgwLmNvbS8iLCJzdWIiOiJhdXRoMHw1OWNlMjJiNDAxOWEzODc5NWIxMWIxNDUiLCJhdWQiOiJRVXZnZGFqTndUQlh3OVp5YXlUUktTZWxHaWl1ZG8wSiIsImlhdCI6MTUwNzEwODE3NCwiZXhwIjoxNTA3MTQ0MTc0fQ.RNy_gXBk-T5qnsWi6uM_GFf8EhR4EkBI0SzedVk3jC8";
				musaUserId = "59ce22b4019a38795b11b145";
			}else{
				console.log("musa.sso.token found: "+window.localStorage["musa.sso.token"]);
				console.log("musa.sso.profile found: "+window.localStorage["musa.sso.profile"]);
				var musaTokenAll = window.localStorage["musa.sso.token"];
				musaToken = musaTokenAll.substring(1, musaTokenAll.length-1);
				var musaProfileAll = JSON.parse(window.localStorage["musa.sso.profile"]);
				musaUserId = musaProfileAll.user_id;
				console.log("musaUserId: "+musaUserId);
			}
			$sessionStorage.musaJwtToken = musaToken;
			$sessionStorage.musaUserId = musaUserId;

			console.log("musa.sso.token after save: "+$sessionStorage.musaJwtToken);
			console.log("musa user id after save: "+$sessionStorage.musaUserId);

			//Functions
			$('[data-toggle=dropdown]').dropdown();
			
			self.setupLocalAppId = setupLocalAppId;
			self.reset = reset;
			self.application = $sessionStorage.application;
			
			$scope.getClass = function (path) {
				return ($location.path().substr(0, path.length) === path) ? 'active' : '';
			}

			if($sessionStorage.application != null){
				self.appId = $sessionStorage.application.kanbanId;
				if(self.appId == null){
					self.appId = $sessionStorage.application.id;
				}
			}

			if($sessionStorage.application == null){
				console.log("Application is null, setup application object!");
				reset();
			}else{
				setupLocalAppId();
			}

			function reset(){
				console.log("Reset call");
				$sessionStorage.application = {id : null, kanbanId : null, name : null, state : null, allSlatSync : false, slaId : null, 
													activeComponent : 
															{id : null, kanbanId : null, name : null, state : null, riskJson : null, metrics : null, slaId : null, slaXml : null, 
																assessedMetrics : null, assessedSlaId : null, assessedSlaXml : null, composedSlaId : null, composedSlaXml : null, 
																hasQuestions : null, slatSync: null, componentGroup: null, threatQuestions : null, threats : null, controls : null},
													components : []};
				self.application = $sessionStorage.application;
				setupLocalAppId();
				$state.go('app_setup');
			}


			function setupLocalAppId(){
				
			}

		}]);