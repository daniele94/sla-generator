'use strict';


		angular.module('slaGenerator').controller('ThreatAgentController',
		['$sessionStorage', 'ThreatAgentService', '$scope', '$location', '$state', '$window',
			function( $sessionStorage, ThreatAgentService, $scope, $location, $state, $window) {

			console.log("ThreatAgentController called");

			var self = this;


			//Variables
			self.application = $sessionStorage.application;
			self.TAQuestions = $sessionStorage.TAQuestions;
			self.TAReplies = $sessionStorage.TAReplies;
			self.replies=$sessionStorage.replies;
			self.Categories=$sessionStorage.Categories;
			self.Attributes=$sessionStorage.Attributes;
			$scope.hashmapCategoryAttribute=new Map();
			$scope.show_table = false;

			self.hashmapCategoryAttribute=$sessionStorage.hashmapCategoryAttribute;


				console.log(self.application);

			//Functions

			self.loadQuestions = loadQuestions;
			self.submitQuestions = submitQuestions;
			self.getReplies=getReplies;
			self.getAttributes=getAttributes;
			self.submitEvaluation=submitEvaluation;

			self.loadQuestions();
			self.getReplies();
			self.getAttributes();


				function loadQuestions(){
				return ThreatAgentService.getQuestions()
				.then(function (response) {
					//alert(JSON.stringify(response));
					//console.log('Questions get successfully '+JSON.stringify(response));
						$sessionStorage.TAQuestions = response;
					self.application = $sessionStorage.application;
					//alert("Questions are successfully loaded!\nPlease answer the questions!");
				},
				function (errResponse) {
					console.error('Error while get Questions ' + errResponse.data.message);
					self.errorMessage = 'Error while get Questions: ' + errResponse.data.message;
					alert('Error while get Questions!\n' + errResponse.data.message);
				}
				);
			}


				function getReplies(){
					return ThreatAgentService.getReplies()
						.then(function (response) {
								//alert(JSON.stringify(response));
								//console.log('Questions get successfully '+JSON.stringify(response));
								$sessionStorage.TAReplies = response;
								//console.log(response);
								self.application = $sessionStorage.application;
								//alert("Questions are successfully loaded!\nPlease answer the questions!");
							},
							function (errResponse) {
								console.error('Error while get Questions ' + errResponse.data.message);
								self.errorMessage = 'Error while get Questions: ' + errResponse.data.message;
								alert('Error while get Questions!\n' + errResponse.data.message);
							}
						);
				}






				function submitQuestions()
				{

					var JSONObject={};
					JSONObject.replies=[];

					console.log("Question Submitted");
					for(var i=0;i<self.TAReplies.length;i++)
					{
						if(self.TAReplies[i].selected==true)
						{

							var JSONObject2={};
							JSONObject2.QuestionId=self.TAReplies[i].question.questionId.toString();
							JSONObject2.reply=self.TAReplies[i].reply.toString();
							JSONObject.replies.push(JSONObject2);
						}
					}
					return ThreatAgentService.submitQuestions(self.application.id,JSONObject)
							.then(function (response) {
									//alert(JSON.stringify(response));
									//console.log('Questions get successfully '+JSON.stringify(response));
									$sessionStorage.Categories = response;
									self.Categories=$sessionStorage.Categories.data;
									$scope.show_table=true;

									self.application = $sessionStorage.application;
									//alert("Questions are successfully loaded!\nPlease answer the questions!");
								},
								function (errResponse) {
									console.error('Error while submitting Questions ' + errResponse.data.message);
									self.errorMessage = 'Error while submitting Questions: ' + errResponse.data.message;
									alert('Error while submitting Questions!\n' + errResponse.data.message);
								}
							);
				}


				//handle checkable inputs
				$scope.manageCheckbox =
					function (TAReply) {
					console.log(TAReply);
					if(TAReply.id==1)
					{
						self.TAReplies[1].selected=false;
					}
					if(TAReply.id==2)
					{
						self.TAReplies[0].selected=false;
					}

					if(TAReply.id==3)
					{
						self.TAReplies[3].selected=false;
					}
					if(TAReply.id==4)
					{
						self.TAReplies[2].selected=false;
					}

					if(TAReply.id==5)
					{
						self.TAReplies[6].selected=false;
					}
					if(TAReply.id==6)
					{
						self.TAReplies[6].selected=false;
					}
					if(TAReply.id==8)
					{
						self.TAReplies[6].selected=false;
					}
					if(TAReply.id==9)
					{
						self.TAReplies[6].selected=false;
					}
					if(TAReply.id==10)
					{
						self.TAReplies[6].selected=false;
					}
					if(TAReply.id==7)
					{
						self.TAReplies[4].selected=false;
						self.TAReplies[5].selected=false;
						self.TAReplies[7].selected=false;
						self.TAReplies[8].selected=false;
						self.TAReplies[9].selected=false;
					}
				};


				function getAttributes()
				{
					return ThreatAgentService.getAttributes()
						.then(function (response) {
								//alert(JSON.stringify(response));
								//console.log('Categories get successfully '+JSON.stringify(response));
								$sessionStorage.Attributes = response;
								self.Attributes=$sessionStorage.Attributes;
								console.log($sessionStorage.Attributes);
								var category;
								var hashmapCategoryAttribute=new Map();
								var keyObj = {};
								//hashmapCategoryAttribute.set(keyObj, "value");
								var scoreLabelArray=[];
								var attribute_label={};

								for(category in self.Attributes)
								{
									keyObj=(category);
									for(var i=0;i<self.Attributes[category].length;i++)
									{
										console.log("Categoria "+category+" ATTRIBUTO "+self.Attributes[category][i].scoreLabel);
										attribute_label["category"]=self.Attributes[category][i].attribute;
										attribute_label["scoreLabel"]=self.Attributes[category][i].scoreLabel;

										scoreLabelArray.push(attribute_label);
										attribute_label={};
									}
									hashmapCategoryAttribute.set(keyObj, scoreLabelArray);
									scoreLabelArray=[];
								}
								$scope.hashmapCategoryAttribute=hashmapCategoryAttribute;

								//$sessionStorage.hashmapCategoryAttribute = hashmapCategoryAttribute;
								//self.hashmapCategoryAttribute=$sessionStorage.hashmapCategoryAttribute;

								self.application = $sessionStorage.application;
								//alert("Questions are successfully loaded!\nPlease answer the questions!");
							},
							function (errResponse) {
								console.error('Error while submitting Questions ' + errResponse.data.message);
								self.errorMessage = 'Error while submitting Questions: ' + errResponse.data.message;
								alert('Error while submitting Questions!\n' + errResponse.data.message);
							}
						);

				}


				function submitEvaluation()
				{

					var JSONObject={};
					JSONObject.scores=[];

					for(var i=0;i<self.Categories.length;i++)
					{
						var JSONObject2={};
						JSONObject2.ThreatAgentName=self.Categories[i].threatAgentCategory.name;
						JSONObject2.score=self.Categories[i].selected;
						JSONObject.scores.push(JSONObject2);

					}
					return ThreatAgentService.submitEvaluation(self.application.id,JSONObject)
						.then(function (response) {
								alert("Threat Agent Score Saved");
								console.log('Results get successfully '+JSON.stringify(response));

								self.application = $sessionStorage.application;
							},
							function (errResponse) {
								console.error('Error while submitting Valutation ' + errResponse.data.message);
								self.errorMessage = 'Error while submitting Questions: ' + errResponse.data.message;
								alert('Error while submitting Valutation\n');
							}
						);
				}









			

			


		}]);