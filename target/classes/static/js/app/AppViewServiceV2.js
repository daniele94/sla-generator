'use strict';

angular.module('slaGenerator').factory('AppViewServiceV2',
		['$sessionStorage','$localStorage', '$http', '$q', 'urls',
			function ($sessionStorage, $localStorage, $http, $q, urls) {

			var factory = {
					setUpComponentById: setUpComponentById,
					updateAppState: updateAppState,
					syncSLATwithComposer: syncSLATwithComposer,
					syncAppWithComposer: syncAppWithComposer,
					setupGraphfromCypher: setupGraphfromCypher,
					addNewComponent: addNewComponent,
					setRequiredSLA: setRequiredSLA,
					setAssessedSLA: setAssessedSLA
			};

			return factory;

			function setRequiredSLA(slaId, componentId, jwtToken, userId){
				var deferred = $q.defer();

				$http(
						{
							method : 'POST',
							url : urls.APPSETUP+"/setRequiredSLA",
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							transformRequest : function(
									obj) {
								var str = [];
								for ( var p in obj)
									str
									.push(encodeURIComponent(p)
											+ "="
											+ encodeURIComponent(obj[p]));
								return str.join("&");
							},
							data : {
								'slaId' : slaId,
								'componentId' : componentId,
								'jwtToken' : jwtToken,
								'userId' : userId
							}

						})
						.then(
								function successCallback(
										response) {
									deferred.resolve(response.data);
								},
								function errorCallback(
										errResponse) {
									deferred.reject(errResponse);
								});

				return deferred.promise;
			}
			
			function setAssessedSLA(slaId, componentId, jwtToken, userId){
				var deferred = $q.defer();

				$http(
						{
							method : 'POST',
							url : urls.APPSETUP+"/setAssessedSLA",
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							transformRequest : function(
									obj) {
								var str = [];
								for ( var p in obj)
									str
									.push(encodeURIComponent(p)
											+ "="
											+ encodeURIComponent(obj[p]));
								return str.join("&");
							},
							data : {
								'slaId' : slaId,
								'componentId' : componentId,
								'jwtToken' : jwtToken,
								'userId' : userId
							}

						})
						.then(
								function successCallback(
										response) {
									deferred.resolve(response.data);
								},
								function errorCallback(
										errResponse) {
									deferred.reject(errResponse);
								});

				return deferred.promise;
			}
			
			function setUpComponentById(appId, componentId, source){
				var deferred = $q.defer();

				$http(
						{
							method : 'POST',
							url : urls.APPSETUP+"/componentFromId",
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							transformRequest : function(
									obj) {
								var str = [];
								for ( var p in obj)
									str
									.push(encodeURIComponent(p)
											+ "="
											+ encodeURIComponent(obj[p]));
								return str.join("&");
							},
							data : {
								'appId' : appId,
								'componentId' : componentId,
								'source' : source,
							}

						})
						.then(
								function successCallback(
										response) {
									deferred.resolve(response.data);
								},
								function errorCallback(
										errResponse) {
									deferred.reject(errResponse);
								});

				return deferred.promise;
			}

			function updateAppState(appId){
				var deferred = $q.defer();

				$http(
						{
							method : 'GET',
							url : urls.APPSETUP+"/applications/state?appId="+appId
						})
						.then(
								function successCallback(
										response) {
									deferred.resolve(response.data);
								},
								function errorCallback(
										errResponse) {
									deferred.reject(errResponse);
								});

				return deferred.promise;
			}

			function syncSLATwithComposer(componentId){
				var deferred = $q.defer();

				$http(
						{
							method : 'POST',
							url : urls.COMPOSER+"/syncSLAT",
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							transformRequest : function(
									obj) {
								var str = [];
								for ( var p in obj)
									str
									.push(encodeURIComponent(p)
											+ "="
											+ encodeURIComponent(obj[p]));
								return str.join("&");
							},
							data : {
								'componentId' : componentId
							}

						})
						.then(
								function successCallback(
										response) {
									deferred.resolve(response.data);
								},
								function errorCallback(
										errResponse) {
									deferred.reject(errResponse);
								});

				return deferred.promise;
			}

			function syncAppWithComposer(appId){
				var deferred = $q.defer();

				$http(
						{
							method : 'POST',
							url : urls.COMPOSER+"/syncApp",
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							transformRequest : function(
									obj) {
								var str = [];
								for ( var p in obj)
									str
									.push(encodeURIComponent(p)
											+ "="
											+ encodeURIComponent(obj[p]));
								return str.join("&");
							},
							data : {
								'appId' : appId
							}
						})
						.then(
								function successCallback(
										response) {
									deferred.resolve(response.data);
								},
								function errorCallback(
										errResponse) {
									deferred.reject(errResponse);
								});

				return deferred.promise;
			}

			function setupGraphfromCypher(CypherContent) {
				console.log('Uploading Cypher ');
				var deferred = $q.defer();

				$http(
						{
							method : 'POST',
							url : urls.COMPOSER+"/setModel",
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							transformRequest : function(
									obj) {
								var str = [];
								for ( var p in obj)
									str
									.push(encodeURIComponent(p)
											+ "="
											+ encodeURIComponent(obj[p]));
								return str.join("&");
							},
							data : {
								'cypher' : CypherContent,
								'model-type': "cypher"
							}

						})
						.then(
								function successCallback(
										response) {
									deferred.resolve(response.data);
								},
								function errorCallback(
										errResponse) {
									deferred.reject(errResponse);
								});

				return deferred.promise;
			}
			
			function addNewComponent(appId, componentName, componentType){
				var deferred = $q.defer();

				$http(
						{
							method : 'POST',
							url : urls.APPSETUP+"/applications/"+appId+"/component/new",
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							transformRequest : function(
									obj) {
								var str = [];
								for ( var p in obj)
									str
									.push(encodeURIComponent(p)
											+ "="
											+ encodeURIComponent(obj[p]));
								return str.join("&");
							},
							data : {
								'compName' : componentName,
								'compType' : componentType
							}

						})
						.then(
								function successCallback(
										response) {
									deferred.resolve(response.data);
								},
								function errorCallback(
										errResponse) {
									deferred.reject(errResponse);
								});

				return deferred.promise;
			}

		}
		]);